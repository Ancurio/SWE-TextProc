\documentclass[a4paper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{array}
\usepackage{shortvrb}
\usepackage{hyperref}

\usepackage[fleqn]{amsmath}
\usepackage{amsfonts}
\usepackage{fullpage}
\usepackage{enumerate}
\usepackage{graphicx}             % import, scale, and rotate graphics
\usepackage{subfigure}            % group figures
\usepackage{alltt}
\usepackage{url}
\usepackage{indentfirst}
\usepackage{eurosym}
\usepackage[german,onelanguage,ruled,vlined]{algorithm2e}

\setcounter{tocdepth}{3}     % Dans la table des matieres
\setcounter{secnumdepth}{3}  % Avec un numero.

\usepackage{hyphenat}
\hyphenation{Text-eingabe Ver-zeich-nisse}
\usepackage{devstyle}		%lstlitings mit farbe usw

\graphicspath{{img/}}

\title{./tep Entwicklerdokumentation}
\author{Gruppe 12}
\date{}

\begin{document}
\renewcommand{\contentsname}{Inhaltsverzeichnis}
\maketitle
\newpage
\tableofcontents
\newpage

\section{Einführung}
Das Textverarbeitungsprogramm tep verfügt über fest eingebaute Verarbeitungsmethoden, welche sich von außen nicht verändern lassen. Die Benutzer haben jedoch die Möglichkeit, das Programm über selbst geschriebene Methoden zu erweitern. Solche Erweiterungen werden nicht in das Programm kompiliert, sondern als externe Dateien im Dateisystem der Benutzer abgelegt, sodass die Installation von Erweiterungen auch nach der Installation des Programms ohne Weiteres möglich ist. Programmiert werden die Erweiterungen in der Sprache "Lua", Version 5.3. Es wird davon ausgegangen, dass die Leserin / der Leser bereits mit der Sprache vertraut sind.
In diesem Dokument werden der Aufbau einer solchen Erweiterung sowie die zur Verfügung stehenden Funktionen erläutert. In Abschnitt \ref{sec:beispiel} wird anhand einer beispielhaften Erweiterung die Entwicklung Schritt für Schritt veranschaulicht.

\section{Datentypen und -konventionen}
\subsection{Strings}
In einer Erweiterung wird zwischen zwei Sorten von Strings (deutsch: Zeichenketten) unterschieden: Lua-Strings und UStrings.
Lua-Strings bezeichnen den nativen Stringtypen von Lua, welcher mit Anführungsstrichen deklariert wird:

\begin{lstlisting}
local str = "ein string"
\end{lstlisting}

Lua-Strings sind jedoch in UTF-8 kodiert, und eignen sich somit nicht für indizierte Zugriffe in konstanter Zeit auf einzelne Zeichen. Um dieses Problem zu umgehen, definiert tep eine eigene Stringklasse namens \lstinline|UString|, welche in UTF-32 kodiert ist. In einem \lstinline|UString| kann auf jedes Unicode Zeichen mittels eines Indexes direkt zugegriffen werden:

\begin{lstlisting}
local ustr = Tep.UString.from_str("ein ßtring mit Unicode")
ustr:get(5)  -- liefert 223, der Unicodewert von "ß"
\end{lstlisting}

Wenn in diesem Dokument lediglich das Wort "String" verwendet wird, kann davon ausgegangen werden, dass beide Typen verwendet werden dürfen.

\subsection{Zeichen}
\label{sec:Datentypen:Zeichen}
Unicodezeichen, wie sie als Parameterwerte übergeben oder mittels der \lstinline|UString| Methoden verwendet werden, sind in Lua als Zahlen repräsentiert. Diese Zahlen entsprechen den jeweiligen Zeichenwerten im Unicode Standard.

\subsection{Indizes}
In Lua werden Indizes, anders als beispielsweise in C++, bei 1 beginnend definiert. Alle von tep definierten Klassen und Funktionen richten sich auch nach dieser Konvention.

\subsection{Bereiche}
Mit "Bereich" ist in diesem Dokument ein Array bestehend aus zwei Indizes (also Zahlen) gemeint, die einen Teilabschnitt eines Strings definieren. Der erste Index zeigt dabei auf das erste Zeichen im Abschnitt, und der zweite Index auf das letzte Zeichen. Es wurden einschließende Indizes gewählt, da dies der natürlichen \lstinline|for|-loop Konvention in Lua entspricht. Dadurch kann man wie folgt über einen Bereich "b" im \lstinline|UString| "ustr" iterieren:

\begin{minipage}{\textwidth}
\begin{lstlisting}
for idx=b[1],b[2] do
  ustr:set(idx, 64) -- Fülle Bereich mit '@'
end
\end{lstlisting}
\end{minipage}

Viele der von Tep definierten Klassen nehmen keine reinen Indizes als Argumente entgegen, sondern arbeiten direkt mit Bereichen.

\section{Struktur einer Erweiterung}
Eine tep Erweiterung besteht aus einer einzigen Lua-Quellcodedatei, es ist keine Einbindung von externen Quellcodes oder Bibliotheken möglich.
Diese Datei gliedert sich in 4 Sektionen:

\begin{itemize}
	\item \textbf{tep\_config}\\
	Eine Table zur Deklaration der notwendigen Parameter (Typ und Name) sowie optionale Hilfstexte
	\item \textbf{tep\_init}\\
	Eine Funktion zur Initialisierung der Erweiterung
	\item \textbf{tep\_process\_line}\\
	Eine Funktion zur zeilenweisen Textverarbeitung
	\item \textbf{tep\_fini}\\
	Eine Funktion zum Abschluss der Textverarbeitung
\end{itemize}

\subsection{tep\_config}
\lstinline|tep_config| muss eine global definierte Variable sein, der eine Table zugewiesen ist. Diese Table definiert alle statischen Informationen, die tep braucht, bevor die Textverarbeitung beginnen kann.

Im Schlüsselwert \lstinline|help| kann optional ein Hilfetext als Lua-String definiert werden, welcher die gesamte Methode genauer beschreibt.

Im Schlüsselwert \lstinline|parameters| muss ein Array definiert sein. Jedes Element dieses Arrays muss eine Table mit folgenden Schlüsseln enthalten:

\begin{itemize}
	\item \textbf{type}\\
	Der Parametertyp (siehe \ref{sec:Parametertypen}) als Lua-String.
	\item \textbf{name}\\
	Der Parametername, welcher bei der benannten Parametrisierung verwendet wird und auch in der Hilfeausgabe erscheint, als Lua-String.
	\item \textbf{values}\\
	Die gültigen enum-Werte in Form eines Arrays aus Lua-Strings. Dieser Schlüssel muss nur für den Parametertypen "enum" definiert sein.
	\item \textbf{help}\\
	Ein optionaler Hilfstext, welcher den Parameter näher beschreibt, als Lua-String.
\end{itemize}

Sollten Hilfetexte definiert sein, so werden diese bei Verwendung des "hilfe" Befehls vom Programm ausgegeben.

\begin{minipage}{\textwidth} % Verhindere Aufspaltung zwischen 2 Seiten
\begin{lstlisting}[caption=tep\_config Beispiel]
tep_config = {
  help = "Spreche einer Person nach",
  parameters = {
    {
      type = "string",
      name = "person",
      help = "Eine Person, der nachgesprochen wird"
    },
    {
      type = "int",
      name = "alter",
      help = "Das Alter der Person"
    }
  }
}
\end{lstlisting}
\end{minipage}


\subsubsection{Parametertypen}
\label{sec:Parametertypen}
\paragraph{string}
Dieser Typ stellt eine einfache Zeichenkette dar, die keiner weiteren Kontrolle unterzogen wird. Er wird als \lstinline|UString| Instanz übergeben.

\paragraph{char}
Dieser Typ stellt ein einzelnes Unicode-Zeichen dar. Er wird als Zahl (siehe \ref{sec:Datentypen:Zeichen}) übergeben.

\paragraph{pattern}
Dieser Typ stellt ein gültiges Muster dar. Er wird als \lstinline|Pattern| Instanz (siehe \ref{sec:API:Pattern}) übergeben.

\paragraph{enum}
Dieser Typ stellt den gewählten Wert aus einer Aufzählung in Form einer Zahl dar. Diese Zahl ist der Index des gewählten Wertes aus dem \lstinline|values| Array.

\paragraph{int}
Dieser Typ stellt eine 32-bit Ganzzahl dar.

\paragraph{intlist}
Dieser Typ stellt ein Array aus 32-bit Ganzzahlen dar. Auf der Kommandozeile wird er wie in der Methode "teile" beschrieben über eine kommagetrennte Liste von Zahlen oder Bereichen übergeben.

\subsection{tep\_init(args)}
\lstinline|tep_init| muss eine global definierte Funktion sein, die einen Parameter annimmt (im Folgenden mit \lstinline|args| bezeichnet). Diese Funktion ist zur generellen Initialisierung von Variablen und Objekten gedacht, welche zur Laufzeit gebraucht werden.

In \lstinline|args| wird eine Table übergeben, die alle in \lstinline|tep_config| deklarierten Parameterwerte enthält; sie nimmt folgende Form an:

\begin{minipage}{\textwidth}
\begin{lstlisting}
{
  { Name = Wert },
  { Name = Wert },
--      ...
}
\end{lstlisting}
\end{minipage}

Wurde also beispielsweise folgender Parameter in \lstinline|tep_config.parameters| deklariert:

\begin{lstlisting}
{ type = "pattern", name = "mein_muster" }
\end{lstlisting}

So kann die dazugehörige \lstinline|Pattern| Instanz wie folgt ausgelesen werden:

\begin{lstlisting}
args.mein_muster
\end{lstlisting}

\subsection{tep\_process\_line(line)}
\lstinline|tep_process_line| muss eine global definierte Funktion sein, die einen Parameter annimmt, in der die aktuelle Eingabezeile als \lstinline|UString| Instanz übergeben wird. In dieser Funktion können die verschiedenen \lstinline|Tep.write_*| Funktionen (siehe \ref{sec:API:Writer}) genutzt werden, um transformierten Text auf die Standardausgabe zu schreiben. Es kann aber auch nichts geschrieben werden, wenn die Methode beispielsweise nur Statistiken über den Text sammelt.

\subsection{tep\_fini()}
\lstinline|tep_fini| muss eine global definierte Funktion sein, die keine Parameter annimmt. Diese Funktion wird aufgerufen, sobald alle Eingabezeilen gelesen wurden und keine weitere Eingabe mehr folgt. In dieser Funktion können die \lstinline|Tep.write_*| Funktionen verwendet werden. Es können beispielsweise gesammelte Statistiken über den Eingabetext oder ein Epilog ausgegeben werden.

\section{Vollständiges Beispiel einer Erweiterung}
\label{sec:beispiel}
Zur Veranschaulichung soll hier Schritt für Schritt die Entwicklung einer beispielhaften Erweiterung erklärt werden. Es soll eine Verarbeitungsmethode geschrieben werden, die ein beliebiges Muster im Eingabetext zensiert. Für die Zensur sollen dem Benutzer zwei hardkodierte Zeichen zur Verfügung stehen: Ein Herz und ein Totenkopf.

Wir beginnen damit, die statischen Daten der Methode in \lstinline|tep_config| zu deklarieren. Als Einleitung kann man mit dem Hilfetext beginnen.

\begin{minipage}{\textwidth}
\begin{lstlisting}
tep_config = {
  help = "Zensiere die Eingabe mit Herzen oder Totenköpfen",
  parameters = {
\end{lstlisting}
\end{minipage}

Aus der Beschreibung ergibt sich sofort, dass der erste Parameter ein Muster wird.

\begin{minipage}{\textwidth}
\begin{lstlisting}
    {
      type = "pattern",
      name = "muster",
      help = "Das zu zensierende Muster"
    },
\end{lstlisting}
\end{minipage}

Wir müssen die Benutzer aber noch zwischen den beiden Zensurzeichen wählen lassen. Wir könnten den Typ "char" verwenden, aber dann wäre das Zensurzeichen entweder nicht mehr hardkodiert, oder wir müssten während zur Laufzeit manuell prüfen, ob ein erlaubtes Zeichen eingegeben wurde. Zudem müssten die Benutzer jedes Mal die richtigen Unicodezeichen parat haben. Viel einfacher ist es, die Benutzer über eine Enumeration auswählen zu lassen.

\begin{minipage}{\textwidth}
\begin{lstlisting}
    {
      type = "enum",
      name = "zeichen",
      values = { "herz", "tod" },
      help = "Das Zeichen, mit dem beleidigende Ausdrücke zensiert werden"
    }
  }
}
\end{lstlisting}
\end{minipage}

Damit haben wir alle nötigen Parameter deklariert. Als nächstes definieren wir zwei globale Variablen, die das Muster und das endgültige Zensurzeichen zur Laufzeit bereithalten werden.

\begin{minipage}{\textwidth}
\begin{lstlisting}
-- Muster, nach dem gesucht wird
muster = nil
-- Zensurzeichen, welches gefundene Muster ersetzt
zensurz = nil
\end{lstlisting}
\end{minipage}

Nun definieren wir \lstinline|tep_init|, um unsere Laufzeitvariablen aus den Parametern zu füllen, die die Benutzer übergeben haben. Da wir den Parameter "zeichen" als "enum" deklariert haben, bekommen wir hier entweder den Wert \lstinline|1| (für "herz") oder \lstinline|2| (für "tod"), und können damit einen \lstinline|UString| indizieren, der beide Zeichen bereithält. \\
(Hinweis: Aus technischen Gründen können die tatsächlichen Unicode-Zeichen in diesem Dokument nicht abgebildet werden und wurden durch "XY" ersetzt. Bitte sehen Sie die mitgelieferte Version der Beispielerweiterung ein).

\begin{minipage}{\textwidth}
\begin{lstlisting}
function tep_init(args)
  muster = args.muster
  
  local tmp = Tep.UString.from_str("XY")
  zensurz = tmp:get(args.zeichen)
end
\end{lstlisting}
\end{minipage}

Bevor wir \lstinline|tep_process_line| definieren, schreiben wir noch eine Hilfsfunktion, die einen Bereich in einem UString mit einem Zeichen füllt. Mittels eines \lstinline|for|-loops können wir über alle Indizes innerhalb des Bereichs iterieren.

\begin{minipage}{\textwidth}
\begin{lstlisting}
function fill(ustr, range, char)
  for idx=range[1],range[2] do
    ustr:set(idx, char)
  end
end
\end{lstlisting}
\end{minipage}

Jetzt müssen wir pro Zeile nur noch alle Vorkomnisse des zu zensierenden Musters finden, und sie mit dem Zeichen auffüllen. Dazu nutzen wir die \lstinline|:find_matches| Methode des Musters.

\begin{minipage}{\textwidth}
\begin{lstlisting}
function tep_process_line(line)
  local vorkom = muster:find_matches(line)
\end{lstlisting}
\end{minipage}

Dann iterieren wir über das Array mit den gefundenen Vorkomnissen. Uns ist die Reihenfolge egal, also nutzen wir pairs. Da ein Vorkomnis bereits in Form eines Bereichs vorliegt, können wir es direkt an unsere fill() Funktion weiterreichen.

\begin{minipage}{\textwidth}
\begin{lstlisting}
  for _,m in pairs(vorkom) do
    fill(line, m, zensurz)
  end
\end{lstlisting}
\end{minipage}

Zuletzt dürfen wir nicht vergessen, den verarbeiteten \lstinline|UString| auch auf die Standardausgabe zu schreiben.

\begin{minipage}{\textwidth}
\begin{lstlisting}
  Tep.write_line(line)
end
\end{lstlisting}
\end{minipage}

Schlussletzlich müssen wir noch die Funktion \lstinline|tep_fini| definieren, auch wenn wir von ihr keinen Gebrauch machen.

\begin{minipage}{\textwidth}
\begin{lstlisting}
function tep_fini()
end
\end{lstlisting}
\end{minipage}

\section{API Referenz}

Im Folgenden werden die Funktionen und Objektmethoden beschrieben, die tep über die gewöhnlichen Lua Konstrukte hinaus bereitstellt.
Dabei werden Funktionen mit dem vollen Aufrufnamen angegeben.
Objektmethoden werden wie folgt notiert:

\begin{lstlisting}
:methode(parameter)
\end{lstlisting}

Der Aufruf dieser Methode eines Objektes \lstinline|obj| sieht dann im tatsächlichen Code wie folgt aus:

\begin{lstlisting}
obj:methode(parameter)
\end{lstlisting}

Der Rückgabewert von Funktionen und Methoden wird mit einem Pfeil (\lstinline|->|) nach dem Namen gekennzeichnet. \lstinline|-> nil| bedeutet hierbei, dass die Funktion bzw. Methode keinen Rückgabewert liefert.

\subsection{UString}
\begin{minipage}{\textwidth}
UStrings sind in UTF-32 kodierte Strings, die Zugriff auf Zeichen in konstanter Zeit erlauben. Mit dem vorangestellten Rautesymbol kann, ähnlich wie bei Lua-Strings und Arrays, die Länge einer \lstinline|UString| Instanz ermittelt werden.

\begin{lstlisting}
local ustr = Tep.UString.from_str("Hallo")
#ustr -- liefert "5"
\end{lstlisting}
\end{minipage}

\subsubsection{Konstruktoren}
\paragraph{}
\begin{lstlisting}
Tep.UString.new(n) -> UString
\end{lstlisting}
Erzeugt eine \lstinline|UString| Instanz, die Platz für n Zeichen hat und mit 0-Werten gefüllt ist.

\paragraph{}
\begin{lstlisting}
Tep.UString.from_str(lstr) -> UString
\end{lstlisting}
Erzeugt eine \lstinline|UString| Instanz aus dem Lua-String "lstr".

\subsubsection{Methoden}
\begin{lstlisting}
:get(i) -> Zeichen
\end{lstlisting}
Gibt das Zeichen an Position "i" zurück.

\paragraph{}
\begin{lstlisting}
:set(i, ch) -> nil
\end{lstlisting}
Schreibt an Position "i" das Zeichen "ch".

\paragraph{}
\begin{lstlisting}
:push(ch) -> nil
\end{lstlisting}
Fügt das Zeichen "ch" am Ende der Instanz an. Die Länge der Instanz wird dadurch um 1 erhöht.

\paragraph{}
\begin{lstlisting}
:append(ustr) -> nil
\end{lstlisting}
Fügt den UString "ustr" am Ende der Instanz an. Die Länge der Instanz wird dadurch um die Länge von "ustr" erhöht.

\paragraph{}
\begin{lstlisting}
:replace(range, ustr) -> nil
\end{lstlisting}
Ersetzt alle Zeichen im Bereich "range" mit dem \lstinline|UString| "ustr". Ein Beispiel, das alle Zeichen vom ersten bis zum fünften mit "Hund" ersetzt:
\begin{lstlisting}
local ustr = Tep.UString.from_str("Heute scheint die Sonne")
local hund = Tep.UString.from_str("Hund")
ustr:replace({1, 5}, hund)
\end{lstlisting}

\paragraph{}
\begin{lstlisting}
:substr(range) -> UString
\end{lstlisting}
Erzeugt einen neuen \lstinline|UString|, der alle Zeichen der Instanz im Bereich "range" enthält.

\subsection{Pattern}
\label{sec:API:Pattern}
\lstinline|Pattern| (deutsch: Muster) stellen reguläre Ausdrücke dar, mit denen \lstinline|UString| Instanzen auf Vorkomnisse untersucht werden können.

\subsubsection{Konstruktoren}
\paragraph{}
\begin{lstlisting}
Tep.Pattern.from_str(str) -> Pattern
\end{lstlisting}
Erzeugt eine \lstinline|Pattern| Instanz aus dem String "str".

\subsubsection{Methoden}
\label{sec:Pattern:find}
\paragraph{}
\begin{lstlisting}
:find_matches(ustr) -> Array
\end{lstlisting}
Durchsucht den \lstinline|UString| "ustr" nach Vorkomnissen. Der Rückgabewert ist ein (möglicherweise leeres) Array aus Bereichen. Ein Beispiel eines Rückgabewertes:

\begin{minipage}{\textwidth}
\begin{lstlisting}
{
  { 2, 5 },
  { 8, 9 },
}
\end{lstlisting}
\end{minipage}

Das bedeutet, dass alle Zeichen zwischen Position 2 und (inklusive) 5 sowie 8 und 9 jeweils ein Vorkomnis der Instanz in "ustr" darstellen.

\subsection{Writer}
\label{sec:API:Writer}
Die Writer-Funktionen erlauben es, verarbeitete Texte auf die Standardausgabe zu schreiben.
Achtung: Diese Funktionen können nicht in der globalen Scope, d.h. außerhalb von \lstinline|tep_init|, \lstinline|tep_proces_line| und \lstinline|tep_fini| aufgerufen werden.

\subsubsection{Funktionen}
\paragraph{}
\begin{lstlisting}
Tep.write_line(ustr) -> nil
\end{lstlisting}
Schreibt den \lstinline|UString| "ustr" mit Zeilenumbruch auf die Standardausgabe.

\paragraph{}
\begin{lstlisting}  
Tep.write_string(ustr) -> nil
\end{lstlisting}
Schreibt den \lstinline|UString| "ustr" auf die Standardausgabe.

\paragraph{}
\begin{lstlisting}
Tep.write_char(ch) -> nil
\end{lstlisting}
Schreibt das Zeichen "ch" auf die Standardausgabe.

\paragraph{}
\begin{lstlisting}
Tep.write_int(n) -> nil
\end{lstlisting}
Schreibt die Ganzzahl "n" auf die Standardausgabe.

\paragraph{}
\begin{lstlisting}
Tep.write_newline() -> nil
\end{lstlisting}
Schreibt den plattform- bzw. dateiabhängigen Zeilenumbruch auf die Standardausgabe.

\subsection{Hilfsfunktionen}

\paragraph{}
\begin{lstlisting}
Tep.read_list_file(str) -> Array
\end{lstlisting}
Ließt die Datei mit String "str" als Pfad aus, und gibt ein Array von \lstinline|UString| Instanzen zurück. Jede Instanz entspricht einer (nicht leeren) Zeile in der ausgelesenen Datei.

\paragraph{}
\begin{lstlisting}
Tep.split_word(ustr) -> Array
\end{lstlisting}
Gibt eine ungefähre Worttrennung der \lstinline|UString| Instanz "ustr" nach den Regeln der deutschen Grammatik in Form eines \lstinline|UString| Arrays zurück.

\paragraph{}
\begin{lstlisting}
Tep.split_string(ustr, ch) -> Array
\end{lstlisting}
Teilt den \lstinline|UString| "ustr" an dem Trennzeichen "ch" auf, und gibt die Teile in Form eines \lstinline|UString| Arrays zurück. Trennzeichen am Anfang und Ende von "ustr" werden ignoriert.

\paragraph{}
\begin{lstlisting}
Tep.debug(...) -> nil
\end{lstlisting}
Gibt eine beliebige Anzahl an Argumenten auf der Diagnoseausgabe aus. Zwischen den Argumenten werden automatisch Leerzeichen eingefügt. Es können alle Standard Luatypen sowie \lstinline|UString| Instanzen ausgegeben werden. Achtung: Unter Windows werden deutsche Umlaute und Eszett vor der Ausgabe nach ASCII (z.B. "ü" -> "ue") konvertiert.

\subsection{Lua Standardfunktionen}
Eine Beschreibung der Lua Standardfunktionen finden Sie unter \url{https://www.lua.org/manual/5.3/}. Folgende Variablen und Funktionen sind in tep verfügbar:

\begin{minipage}{\textwidth}
\begin{itemize}
    \item \lstinline|_G|
	\item \lstinline|ipairs|
	\item \lstinline|pairs|
	\item \lstinline|next|
	\item \lstinline|pcall|
	\item \lstinline|rawequal|
	\item \lstinline|rawlen|
	\item \lstinline|rawget|
	\item \lstinline|rawset|
	\item \lstinline|select|
	\item \lstinline|setmetatable|
	\item \lstinline|tonumber|
	\item \lstinline|tostring|
	\item \lstinline|type|
	\item \lstinline|xpcall|
	\item \lstinline|table.concat|
	\item \lstinline|table.insert|
	\item \lstinline|table.pack|
	\item \lstinline|table.unpack|
	\item \lstinline|table.remove|
	\item \lstinline|table.move|
	\item \lstinline|table.sort|
\end{itemize}
\end{minipage}

\end{document}
