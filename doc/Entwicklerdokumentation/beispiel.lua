tep_config = {
  help = "Zensiere die Eingabe mit Herzen oder Totenköpfen",
  parameters = {
    {
      type = "pattern",
      name = "muster",
      help = "Das zu zensierende Muster"
    },
    {
      type = "enum",
      name = "zeichen",
      values = { "herz", "tod" },
      help = "Das Zeichen, mit dem beleidigende Ausdrücke zensiert werden"
    }
  }
}

-- Muster, nach dem gesucht wird
muster = nil
-- Zensurzeichen, welches gefundene Muster ersetzt
zensurz = nil

function tep_init(args)
  muster = args.muster

  local tmp = Tep.UString.from_str("❤💀")
  zensurz = tmp:get(args.zeichen)
end

function fill(ustr, range, char)
  for idx=range[1],range[2] do
    ustr:set(idx, char)
  end
end

function tep_process_line(line)
  local vorkom = muster:find_matches(line)
  for _,m in pairs(vorkom) do
    fill(line, m, zensurz)
  end

  Tep.write_line(line)
end

function tep_fini()
end
