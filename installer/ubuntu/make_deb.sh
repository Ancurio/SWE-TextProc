#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 [tep-binary]"
  exit 1
fi

mkdir -p tep_1.0/usr/local/bin
cp "${1}" tep_1.0/usr/local/bin/tep
dpkg-deb --build tep_1.0
