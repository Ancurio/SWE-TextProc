#!/bin/bash

INST_NAME=tep_osx_install.sh
cat install_prolog <(gzip -c "${1:-tep}" | openssl base64) install_epilog > $INST_NAME
chmod +x $INST_NAME
