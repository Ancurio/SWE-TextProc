#!/bin/bash

PLUGIN_PATH="/usr/local/lib/tep/"
EXE_PATH="/usr/local/bin/"

echo "Entferne Verzeichnis $PLUGIN_PATH"
rmdir $PLUGIN_PATH

echo "Entferne ${EXE_PATH}tep"
rm -f ${EXE_PATH}tep
