# Gruppenprojekt SWE
Code in C++11  
Bitte keine System("PAUSE"); usw benutzen

## Verzeichnisstruktur
**src/**  
Quellcode des Hauptprogramms

**src/kern/**  
Kernfunktionalitäten, die allen Methoden zur Verfügung stehen  

**src/methoden/**  
Verarbeitungsmethoden (jeweils eine .cpp)

**extern/**  
Quellcode von Fremdprojekten, die in das Hauptprogramm
eingebunden werden (boost, Lua etc.)

**cmake/**  
Ausgelagerte cmake Funktionen

**doc/**  
Dokumentation / Notizen zur Architektur (später vielleicht auch
Benutzerdoku)

**test/extern/**  
Tests, die das kompilierte Hauptprogramm aufrufen (wie eine
Shell) und Eingabe/Ausgabe für alle Methoden prüfen  

**test/intern/**  
Tests, die mit Teilen des Kernquellcodes zusammen
kompiliert werden und Code testen, der nach Außen nicht direkt sichtbar
ist (Argument parsing, Wortteiler etc.)  
