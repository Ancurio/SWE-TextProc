#include <cstdint>
#include <string>
#include <vector>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <ostream>
#include <iomanip>
#include <cassert>
#include "main.h"
#include "../test/intern/worttrennung/trennungen.h"
#include "hashset.h"
#include "utf.h"

#define HASH_SIZE 0x40000ULL

using namespace tep;

struct BitSet {
  std::vector<uint8_t> bytes;
  hashset::HashFunc func;
  size_t collCount;

  BitSet(size_t bitCount, hashset::HashFunc func)
    : bytes(bitCount / 8, 0),
      func(func),
      collCount(0) {
  }

  void put(const std::u32string &str) {
    uint8_t mask;
    uint8_t &byte = hashset::getBit(str, bytes, func, mask);

    if (byte & mask) {
      ++collCount;
      return;
    }

    byte = byte | mask;
  }

  bool contains(const std::u32string &str) {
    uint8_t mask;
    const uint8_t &byte = hashset::getBit(str, bytes, func, mask);

    return (byte & mask);
  }

  size_t byteCount() const {
    return bytes.size();
  }
};

struct Data {
  BitSet &set1;
  BitSet &set2;
  size_t minLen;
  size_t maxLen;
  size_t maxParts;
};

static void genHashSetCpp(const BitSet &set,
                          const std::string &symName,
                          std::ostream &cpp) {
  cpp << "const std::array<uint8_t, " << set.byteCount() << "> "
      << symName << " {";

  int linePos = 8;
  cpp << std::hex << std::uppercase << std::setfill('0');

  for (unsigned byte : set.bytes) {
    if (++linePos >= 8) {
      linePos = 0;
      cpp << "\n ";
    }

    cpp << " 0x" << std::setw(2) << byte << ',';
  }

  cpp << std::dec << "\n};\n";
}

static void genHashSetH(const BitSet &set,
                          const std::string &symName,
                          std::ostream &h) {
  h << "static constexpr size_t " << symName << "Size = "
    << set.byteCount() << ";\n";
  h << "extern const std::array<uint8_t, " << set.byteCount()
    << "> " << symName << ";\n";
}

static void generateSrc(const Data &data,
                        const std::string &symbolName,
                        const std::string &filenameH,
                        std::ostream &cpp,
                        std::ostream &h) {
  h << "#include <cstdint>\n";
  h << "#include <cstddef>\n";
  h << "#include <array>\n";
  h << '\n';
  h << "namespace tep {\n";
  h << '\n';
  h << "static constexpr size_t " << symbolName << "MinLen = " << data.minLen << ";\n";
  h << "static constexpr size_t " << symbolName << "MaxLen = " << data.maxLen << ";\n";
  h << "\n";
  genHashSetH(data.set1, symbolName + "1", h);
  genHashSetH(data.set2, symbolName + "2", h);
  h << "\n}\n";

  cpp << "#include \"" << filenameH << "\"\n";
  cpp << "\n";
  cpp << "namespace tep {\n";
  cpp << '\n';

  genHashSetCpp(data.set1, symbolName + "1", cpp);
  cpp << '\n';
  genHashSetCpp(data.set2, symbolName + "2", cpp);
  cpp << '\n';

  cpp << "\n}\n";
}

static void printStats(const BitSet &set, size_t totalCount) {
  std::cerr << "Collisions (total): " << set.collCount << " ("
            << totalCount << ")" << std::endl;
  std::cerr << "Collision rate: "
            << ((float) set.collCount / totalCount) * 100
            << "%" << std::endl;
  std::cerr << "Bitset size: " << set.byteCount() << " bytes"
            << std::endl;
}

int tep::main(const std::vector<std::u32string>&) {
  const char *p = test::trennungen;
  BitSet bitset1(HASH_SIZE, hashset::hash1);
  BitSet bitset2(HASH_SIZE, hashset::hash2);
  std::unordered_set<std::string> wordParts;

  Data data{ bitset1, bitset2, UINT32_MAX, 0, 0 };

  int dupCount = 0;
  size_t partCount = 1;

  std::string longestPart;

  while (*p) {
    std::string wordPart;

    for (; *p && *p != '|' && *p != ';'; ++p) {
      wordPart.push_back(*p);
    }

    ++p;

    if (wordParts.find(wordPart) != wordParts.cend()) {
      ++dupCount;
      continue;
    }

    wordParts.insert(wordPart);

    if (wordPart.size() > data.maxLen) {
      data.maxLen = wordPart.size();
      longestPart = wordPart;
    }

    if (wordPart.size() < data.minLen) {
      data.minLen = wordPart.size();
    }

    std::u32string u32 = utf::convert(wordPart);

    data.set1.put(u32);
    data.set2.put(u32);

    if (*p == '|') {
      ++partCount;
    }
    else if (*p == ';') {
      if (partCount > data.maxParts) {
        data.maxParts = partCount;
      }

      partCount = 1;
    }
  }

  for (const auto &iter : wordParts) {
    std::u32string u32 = utf::convert(iter);
    assert(bitset1.contains(u32));
    assert(bitset2.contains(u32));
  }

  std::cerr << "Longest syllable: " << longestPart << std::endl;
  std::cerr << "Duplicates: " << dupCount << std::endl;

  std::cerr << "Hashset1:" << std::endl;
  printStats(bitset1, wordParts.size());
  std::cerr << "Hashset2:" << std::endl;
  printStats(bitset2, wordParts.size());

  const std::string filename("hyph-bitset");
  const std::string filenameCpp = filename + ".cpp";
  const std::string filenameH = filename + ".h";

  std::ofstream cppFile(filenameCpp);
  std::ofstream hFile(filenameH);
  generateSrc(data, "hyphBitset", filenameH, cppFile, hFile);
  cppFile.close();
  hFile.close();

  return 0;
}
