# Add all listed filenames to SOURCES. The filenames are relative
# to the current source directory, and should NOT contain leading
# directory names.
function (add_subsources)
  foreach (FILE ${ARGV})
    set (SOURCES ${SOURCES}
         ${CMAKE_CURRENT_SOURCE_DIR}/${FILE} CACHE STRING "" FORCE)
  endforeach (FILE)
endfunction (add_subsources)

function (add_subheaders)
  foreach (FILE ${ARGV})
	set (HEADERS ${HEADERS}
		 ${CMAKE_CURRENT_SOURCE_DIR}/${FILE} CACHE STRING "" FORCE)
  endforeach (FILE)
endfunction (add_subheaders)

# Add the current source directory to the INCLUDE search path.
function (add_subinclude)
  set (INCLUDES ${INCLUDES} ${CMAKE_CURRENT_SOURCE_DIR} CACHE STRING "" FORCE)
endfunction (add_subinclude)

# Add all listed filenames to the list of
# external tests to be run with testrunner
function (add_tests)
  foreach (FILE ${ARGV})
    configure_file (${FILE} ${FILE} COPYONLY)
  endforeach (FILE)
endfunction (add_tests)
