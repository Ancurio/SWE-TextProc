#include <iostream>
#include <memory>
#include <cstdlib>
#include <cassert>
#include "main.h"
#include "method-registry.h"
#include "parameter.h"
#include "argvparser.h"
#include "utf.h"
#include "io.h"
#include "help.h"
#include "plugin.h"

using namespace tep;

static void runMainLoop(method::Base::Handle &meth,
                        utf::Writer &writer) {
  std::u32string line;
  utf::Reader reader(std::cin, writer);

  /* Lese Zeile für Zeile ein, verarbeite, und gebe wieder aus */
  while (reader.getLine(line)) {
    meth->processLine(line);
  }

  meth->finalize();
}

int tep::main(const std::vector<std::u32string> &args) {
  /* Dies ist nur ein Platzhalter für die spätere main Implementierung */

  if (args.size() < 2) {
    /* Das Programm wurde ohne Parameter aufgerufen */
    help::generalUsage();
    return 0;
  }

  const std::u32string &commandName = args[1];

  if (commandName == help::helpCom) {
    if (args.size() == 2) {
      help::commandUsage();
    } else {
      help::commandUsage(args[2]);
    }

    return 0;
  }

  utf::Writer writer(std::cout);
  method::Base::ConstructParam methodParam(writer);

  if (commandName == help::testCom) {
    if (args.size() == 2) {
      help::testUsage();
      return 0;
    }

    try {
      std::string filename = utf::convert(args[2]);
      plugin::Module::Handle pl;
      pl.reset(new plugin::Module(filename, std::u32string()));

      const method::Desc &desc = pl->getDesc();
      std::vector<std::u32string> methArgs(args.cbegin() + 3, args.cend());

      if (desc.param.size() > 0 && methArgs.empty()) {
        help::methodParamUsage(desc.param);
        return 0;
      }

      method::Base::ConstructParam cp(writer);
      argv::parse(methArgs, desc.param, cp.param);

      auto meth = pl->construct(cp);
      runMainLoop(meth, writer);

    } catch (const std::exception &exc) {
      io::Debug() << "Fehler in der Erweiterung:";
      io::Debug() << exc.what();
    }

    return 0;
  }

  if (!method::Registry::hasMethod(commandName)) {
    io::Debug().noSpaces() << "Unterbefehl '" << args[1] << "' existiert nicht";
    help::printCommandList();

    return 0;
  }

  auto methDesc = method::Registry::getMethodDesc(commandName);

  try {
    std::vector<std::u32string> arguments(args.cbegin() + 2, args.cend());
    argv::parse(arguments, methDesc.param, methodParam.param);
  } catch (const argv::Error &exc) {
    io::Debug() << "Fehlerhafte Parameter:";
    io::Debug() << exc.what();
    io::Debug() << "";

    help::methodParamUsage(methDesc.param);

    return 0;
  }

  /* Erzeuge eine Instanz des ausgewählten Moduls */
  method::Base::Handle mod;

  try {
    mod = method::Registry::constructMethod(commandName, methodParam);
  } catch (const std::exception &exc) {
    io::Debug() << "Ein Fehler in der Methode ist entstanden:";
    io::Debug() << exc.what();

    return 0;
  }

  assert(mod != nullptr);

  runMainLoop(mod, writer);

  return 0;
}
