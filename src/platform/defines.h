#ifndef DEFINES_H
#define DEFINES_H

#if defined(TEP_TARGET_UNIX)

# define PLUGIN_PATH "/usr/local/lib/tep"
# define PATH_DELIM '/'
# define DEV_NULL "/dev/null"
# define TEP_EXE  "./tep"
# define NATIVE_LF "\n"

#elif defined(TEP_TARGET_WIN32)

# define PLUGIN_PATH "C:\\tep\\plugins"
# define PATH_DELIM '\\'
# define DEV_NULL "nul"
# define TEP_EXE  "tep.exe"
# define NATIVE_LF "\r\n"

#endif

#endif // DEFINES_H
