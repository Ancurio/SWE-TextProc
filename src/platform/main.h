#ifndef MAIN_H
#define MAIN_H

#include <vector>
#include <string>
#include "utf.h"

/* Der Eintrittspunkt einer ausführbaren Datei
 * im tep Projekt (ersetzt main()) */
namespace tep {
int main(const std::vector<std::u32string> &args);
}

#ifdef TEP_TARGET_WIN32
# ifndef _UNICODE
#  define _UNICODE
# endif
# ifndef UNICODE
#  define UNICODE
# endif
# include <wchar.h>
# include <codecvt>
# include <locale>
#endif

#if defined(TEP_TARGET_WIN32)

namespace tep {
namespace utf {

/* Konvertiere UTF-16 -> UTF-32 */
std::u32string convert(const std::wstring &str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conv;
    return convert(conv.to_bytes(str));
}

}
}

extern "C"
int wmain(int argc, wchar_t *argv[])

#elif defined(TEP_TARGET_UNIX)

int main(int argc, char *argv[])

#endif

{
  std::vector<std::u32string> args;

  for (int i = 0; i < argc; ++i)
    args.push_back(tep::utf::convert(argv[i]));

  return tep::main(args);
}

#endif // MAIN_H
