#ifndef HASHSET_H
#define HASHSET_H

#include <cstdint>
#include <string>

namespace tep {
namespace hashset {

typedef uint64_t (*HashFunc)(const std::u32string &str);

uint64_t hash1(const std::u32string &str);
uint64_t hash2(const std::u32string &str);

inline uint32_t
getByteOffset(const std::u32string &str, size_t arraySize,
              HashFunc func, uint8_t &mask)
{
  uint64_t hashVal = func(str);
  hashVal %= (arraySize * 8);
  mask = 1 << (hashVal % 8);

  return hashVal / 8;
}

/* Gibt den Offset im byteArray zurück, sowie die Maske,
 * um das Bit anzuwählen */
template<class A>
inline uint8_t&
getBit(const std::u32string &str, A &byteArray,
                       HashFunc func, uint8_t &mask) {
  uint32_t off = getByteOffset(str, byteArray.size(), func, mask);
  return byteArray[off];
}

template<class A>
inline const uint8_t&
getBit(const std::u32string &str, const A &byteArray,
                       HashFunc func, uint8_t &mask) {
  uint32_t off = getByteOffset(str, byteArray.size(), func, mask);
  return byteArray[off];
}

}
}

#endif // HASHSET_H
