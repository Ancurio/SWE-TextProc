#ifndef REGEX_CHAR32_TRAITS_H
#define REGEX_CHAR32_TRAITS_H

#include <map>
#include <boost/xpressive/traits/null_regex_traits.hpp>
#include "util.h"

namespace xpr = boost::xpressive;

struct char32_regex_traits : xpr::null_regex_traits<char32_t> {
  struct locale_type {};

  enum char_class {
    InvalidClass = 0,
    Digit = (1 << 0),
    Lower = (1 << 1),
    Upper = (1 << 2),
    Alpha = Lower | Upper,
    Alnum = Digit | Alpha,
    Space = (1 << 3),

    /* Wird nur intern verwendet:
     * 'UndSc' erkennt nur ein '_' */
    UndSc = (1 << 4),
    Word = Alnum | UndSc,
  };

  typedef boost::uint_value_t<Space>::least char_class_type;

  char32_regex_traits(locale_type = locale_type()) {
  }

  /* Übersetze einen Klassennamen als String in die
   * dazugehörige Bitmask */
  template<typename FwdIter>
  static char_class_type lookup_classname(FwdIter begin, FwdIter end, bool icase)
  {
    xpr::detail::ignore_unused(icase);
    std::u32string name(begin, end);

    static const std::map<std::u32string, char_class_type> charClasses = {
      { U"digit", Digit },
      { U"lower", Lower },
      { U"upper", Upper },
      { U"alpha", Alpha },
      { U"alnum", Alnum },
      { U"space", Space },

      /* Wird intern verwendet, um das '\b'
       * (word boundary) Metazeichen zu implementieren */
      { U"w",     Word  },
    };

    auto iter = charClasses.find(name);

    if (iter == charClasses.end()) {
      return InvalidClass;
    }

    return (*iter).second;
  }

  /* Prüfe, ob ein Zeichen einer Klassenbitmask entsprich */
  static bool isctype(char_type ch, char_class_type mask)
  {
    xpr::detail::ignore_unused(ch);
    xpr::detail::ignore_unused(mask);

    bool digit = (ch >= U'0' && ch <= U'9');

    bool lower = tep::util::isGermanLower(ch);
    bool upper = tep::util::isGermanUpper(ch);

    bool space = (ch == U' ' || ch == U'\t');

    return ((mask & Digit) && (digit)) ||
           ((mask & Lower) && (lower)) ||
           ((mask & Upper) && (upper)) ||
           ((mask & Space) && (space)) ||
           ((mask & UndSc) && (ch == U'_'));

    return false;
  }

  static int value(char32_t ch, int radix)
  {
    if (radix != 10)
      return -1;

    if (ch < U'0' || ch > U'9')
      return -1;

    return ch - U'0';
  }
};

#endif // REGEX_CHAR32_TRAITS_H
