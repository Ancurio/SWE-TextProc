#include "hyphenation.h"
#include "lexical-algo.h"

namespace tep {

HyphList splitWord(const std::u32string &word) {
  return hyph::lexicalSplit(word);
}

}
