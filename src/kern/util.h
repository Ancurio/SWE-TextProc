#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>

namespace tep {
namespace util {

/* Prüfe, ob 'ch' ein deutscher Buchstabe ist */
bool isGermanLetter(char32_t ch);

/* Prüfe, ob 'ch ein deutscher Großbuchstabe ist */
bool isGermanLower(char32_t ch);

/* Prüfe, ob 'ch ein deutscher Kleinbuchstabe ist */
bool isGermanUpper(char32_t ch);

char32_t toGermanLower(char32_t ch);
char32_t toGermanUpper(char32_t ch);

std::u32string toGermanLower(const std::u32string &str);

/* Teile 's' an 'delim' auf; 'delim' kann auch am Anfang
 * und Ende von 's' vorkommen und wird ignoriert */
std::vector<std::u32string>
split(const std::u32string &str, char32_t delim);

}
}

#endif // UTIL_H
