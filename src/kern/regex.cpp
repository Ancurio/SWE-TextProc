#include <stdexcept>
#include <map>
#include <new>
#include <boost/xpressive/regex_compiler.hpp>
#include <boost/xpressive/regex_constants.hpp>
#include <boost/xpressive/basic_regex.hpp>
#include <boost/xpressive/regex_algorithms.hpp>
#include <boost/xpressive/match_results.hpp>
#include "regex.h"
#include "regex-char32-traits.h"

typedef std::u32string::const_iterator u32citer;
typedef std::u32string::size_type u32size;
typedef xpr::regex_compiler<u32citer, char32_regex_traits> xprCompiler;
typedef xpr::basic_regex<u32citer> xprRegex;

namespace tep {
namespace regex {

Pattern::Pattern() {
}

Pattern::~Pattern() {
}

namespace xprc = xpr::regex_constants;

/* Versuche, zumindest die geläufigsten Fehler abzudecken */
static const std::map<xprc::error_type, std::string> errorStrings = {
  { xprc::error_ctype,     "Unbekannte Zeichenklasse"                },
  { xprc::error_escape,    "Ungültige Escape-Sequenz"                },
  { xprc::error_subreg,    "Ungültiger Rückverweis"                  },
  { xprc::error_brack,     "Eckige Klammer [ nicht geschlossen"      },
  { xprc::error_paren,     "Runde Klammer ( nicht geschlossen"       },
  { xprc::error_brace,     "Geschweifte Klammer { nicht geschlossen" },
  { xprc::error_badbrace,  "Ungültiger Bereich innerhalb {}"         },
  { xprc::error_range,     "Ungültiger Zeichenbereich innerhalb []"  },
  { xprc::error_badrepeat, "Ungültiger Ausdruck vor Quantor"         },
  { xprc::error_internal,  "Interner Fehler"                         },
};

class PatternImpl : public Pattern {
friend class Pattern;

  PatternImpl(const std::u32string &desc) {
    xprCompiler comp;
    xprRegex::flag_type flags = xpr::regex_constants::ECMAScript
                              | xpr::regex_constants::optimize;

    try {
      re = comp.compile(desc.cbegin(), desc.cend());
    } catch (const xpr::regex_error &exc) {
      auto errStr = errorStrings.find(exc.code());

      if (errStr != errorStrings.cend()) {
        throw std::runtime_error(errStr->second);
      } else {
        throw std::runtime_error(exc.what());
      }
    }
  }

  PatternImpl() {
  }

  std::vector<Range> findMatches(const std::u32string &str) {
    std::vector<Range> matches;
    xpr::match_results<u32citer> results;

    /* Anfang der noch zu durchsuchenden Teilzeichenkette */
    u32citer begin = str.cbegin();
    /* Ende der Zeichenkette */
    const u32citer end = str.cend();

    /* Anfang der gefundenen Teilzeichenkette */
    u32size subBegin = 0;
    /* Ende der gefundenen Teilzeichenkette */
    u32size subEnd;

    while (xpr::regex_search(begin, end, results, re) && begin != end) {
      u32size pos = results.position();
      u32size len = results.length();

      subBegin += pos;
      subEnd = subBegin + len;

      matches.push_back(Range{subBegin, subEnd});

      begin = str.cbegin() + subEnd;
      subBegin = subEnd;
    }

    return matches;
  }

  void swap(Pattern &o) {
    PatternImpl &oImpl = dynamic_cast<PatternImpl&>(o);
    re.swap(oImpl.re);
  }

  xprRegex re;
};

Pattern::Handle Pattern::construct(const std::u32string &desc) {
  return Pattern::Handle(new PatternImpl(desc));
}

size_t Pattern::implSize() {
  return sizeof(PatternImpl);
}

void Pattern::implConstruct(void *p) {
  new (p) PatternImpl();
}

}
}
