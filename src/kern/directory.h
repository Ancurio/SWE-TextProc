#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <string>
#include <vector>

namespace tep {
namespace directory {

std::vector<std::string> collectFileList(const std::string &root);

}
}

#endif // DIRECTORY_H
