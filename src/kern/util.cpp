#include "util.h"

namespace tep {
namespace util {

bool isGermanLower(char32_t ch) {
  if (ch >= U'a' && ch <= U'z') {
    return true;
  }

  switch (ch) {
  case U'ä':
  case U'ö':
  case U'ü':
  case U'ß':
    return true;

  default:
    return false;
  }
}

bool isGermanUpper(char32_t ch) {
  if (ch >= U'A' && ch <= U'Z') {
    return true;
  }

  switch (ch) {
  case U'Ä':
  case U'Ö':
  case U'Ü':
    return true;

  default:
    return false;
  }
}

bool isGermanLetter(char32_t ch) {
  return isGermanLower(ch) || isGermanUpper(ch);
}

char32_t toGermanLower(char32_t ch) {
  static const char32_t shift = (U'a' - U'A');

  if (!isGermanUpper(ch)) {
    return ch;
  }

  return ch + shift;
}

char32_t toGermanUpper(char32_t ch) {
  static const char32_t shift = (U'a' - U'A');

  if (!isGermanLower(ch) || ch == U'ß') {
    return ch;
  }

  return ch - shift;
}

std::u32string toGermanLower(const std::u32string &str) {
  std::u32string result = str;

  for (char32_t &ch : result) {
    ch = toGermanLower(ch);
  }

  return result;
}

std::vector<std::u32string>
split(const std::u32string &str, char32_t delim) {
  std::vector<std::u32string> result;
  auto iter = str.begin();
  const auto end = str.end();

  /* Fange leeren string ab */
  if (iter == end)
    return result;

  /* Fange Randfall "erstes Zeichen ist Trennzeichen" ab.
   * Dadurch zeigt iter zu Beginn der Schleife immer auf
   * das erste Zeichen des nächsten Teils, oder auf delim
   * falls das Teil leer ist */
  if (*iter == delim)
    ++iter;

  while (iter != end) {
    /* Puffer für ein Teil */
    std::u32string piece;

    /* iter zeigt jetzt immer auf ein Zeichen das kein
     * Trennzeichen ist, und ist auch nicht s.end() */
    for (; *iter != delim && iter != end; ++iter) {
      piece.push_back(*iter);
    }

    result.push_back(piece);

    if (*iter == delim)
      ++iter;
  }

  return result;
}

}
}
