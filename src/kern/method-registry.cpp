#include <cassert>
#include <algorithm>
#include "method-registry-intern.h"
#include "plugin.h"

namespace tep {
namespace method {

static std::map<std::u32string, const Factory*>*
methFactories = nullptr;

static std::map<std::u32string, plugin::Module::Handle> loadedPlugins;

Base::Base(ConstructParam &p)
  : out(p.out) {
}

Base::~Base() {
}

void Base::processLine(const std::u32string&) {
  assert(!"processLine nicht implementiert");
}

void Base::finalize() {
}

Factory::Factory(const std::u32string &methodName)
  : name(methodName) {
  Registry::registerMethod(*this, name);
}

Factory::~Factory() {
  Registry::unregisterMethod(name);
}

void Registry::registerMethod(const Factory &reg,
                              const std::u32string &name) {
  if (!methFactories) {
    methFactories = new std::map<std::u32string, const Factory*>();
  }

  (*methFactories)[name] = &reg;
}

void Registry::unregisterMethod(const std::u32string &name) {
  assert(methFactories);
  auto iter = methFactories->find(name);

  assert(iter != methFactories->end());
  methFactories->erase(iter);

  /* Sind alle Factories gelöscht, so gib auch die
   * std::map frei */
  if (methFactories->empty()) {
    delete methFactories;
    methFactories = nullptr;
  }
}

std::vector<std::u32string> Registry::getMethodList() {
  std::vector<std::u32string> list;

  /* Sind keine Methoden registriet, so ist
   * die std::map auch noch nicht erzeugt worden */
  if (!methFactories) {
    return list;
  }

  for (auto m : (*methFactories)) {
    list.push_back(m.first);
  }

  auto plugins = plugin::listInstalled();

  for (const auto m : plugins) {
    list.push_back(m.first);
  }

  std::sort(list.begin(), list.end());

  return list;
}

bool Registry::hasMethod(const std::u32string &name) {
  if (methFactories->find(name) == methFactories->end()) {
    auto plugins = plugin::listInstalled();
    return (plugins.find(name) != plugins.cend());
  }

  return true;
}

static Base::Handle
constructPluginMethod(const std::u32string &name,
                      Base::ConstructParam &param) {
  auto iter = loadedPlugins.find(name);

  if (iter == loadedPlugins.cend()) {
    return nullptr;
  }

  plugin::Module::Handle &p = iter->second;
  return p->construct(param);
}

Base::Handle
Registry::constructMethod(const std::u32string &name,
                          Base::ConstructParam &param) {
  if (!methFactories) {
    return nullptr;
  }

  auto iter = methFactories->find(name);

  if (iter == methFactories->end()) {
    return constructPluginMethod(name, param);
  }

  return (*iter).second->construct(param);
}

// XXX should throw instead
static const Desc defaultDesc{U""};

static const Desc
&tryGetPluginDesc(const std::u32string &name) {
  auto installed = plugin::listInstalled();
  auto iter = installed.find(name);

  if (iter == installed.cend()) {
    return defaultDesc;
  }

  plugin::Module::Handle p;
  p.reset(new plugin::Module(iter->second, name));
  const Desc &result = p->getDesc();

  loadedPlugins.insert(std::make_pair(name, std::move(p)));

  return result;
}

const Desc
&Registry::getMethodDesc(const std::u32string &name) {
  if (!methFactories) {
    return defaultDesc;
  }

  auto iter = methFactories->find(name);

  if (iter == methFactories->end()) {
    return tryGetPluginDesc(name);
  }

  return (*iter).second->getDesc();
}

}
}
