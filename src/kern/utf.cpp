#include "utf.h"

#include <locale>
#include <codecvt>
#include "defines.h"

namespace tep {
namespace utf {

std::u32string convert(const std::string &str)
{
  std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
  return conv.from_bytes(str);
}

std::string convert(const std::u32string &str)
{
  std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
  return conv.to_bytes(str);
}

Writer::Writer(std::ostream &stream)
  : stream(stream), linefeed(NATIVE_LF) {

}

void Writer::writeLine(const std::u32string &line) {
  stream << convert(line) << linefeed;
}

void Writer::writeString(const std::u32string &str) {
  stream << convert(str);
}

void Writer::writeChar(char32_t ch) {
  stream << convert(std::u32string(1, ch));
}

void Writer::writeUInt(uint64_t i) {
  stream << i;
}

void Writer::writeInt(int64_t i) {
  stream << i;
}

void Writer::writeNewline() {
  stream << linefeed;
}

Reader::Reader(std::istream &stream,
               Writer &writer)
  : stream(stream), writer(writer) {
}

bool Reader::getLine(std::u32string &str) {
  char ch = '\0';
  std::string buffer;

  stream.get(ch);

  if (stream.eof()) {
    return false;
  }

  while (true) {
    if (ch == '\n') {
      /* Unix newline */
      writer.linefeed = "\n";
      break;
    }

    if (ch == '\r') {
      char next = '\0';
      stream.get(next);

      if (next == '\n') {
        /* Win32 newline */
        writer.linefeed = "\r\n";
        break;
      } else {
        /* Old macOS newline */
        if (!stream.eof()) {
          stream.unget();
        }

        writer.linefeed = "\r";
        break;
      }
    }

    buffer.push_back(ch);
    stream.get(ch);

    if (stream.eof()) {
      break;
    }
  }

  str = utf::convert(buffer);
  return true;
}

}
}
