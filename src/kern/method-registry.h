#ifndef METHOD_REGISTRY_H
#define METHOD_REGISTRY_H

#include <vector>
#include <map>
#include <string>
#include <memory>
#include "parameter.h"

namespace tep {

namespace utf {
class Writer;
}

namespace method {

struct Desc {
  std::u32string help;
  std::vector<param::Desc> param;
};

class Base {
public:
  struct ConstructParam {
    ConstructParam(utf::Writer &out)
      : out(out) {}

    std::vector<param::Value> param;
    utf::Writer &out;
  };

  typedef std::unique_ptr<Base> Handle;

  virtual ~Base();

  /* Verarbeite eine Zeile des Eingabetexts. Dabei kann,
   * muss aber nicht, auch etwas auf die Ausgabe geschrieben
   * werden. */
  virtual void processLine(const std::u32string &line);

  /* Wird aufgerufen, wenn alle Eingabezeilen abgearbeitet
   * wurden und die Eingabe zu Ende ist. Hier können z.B.
   * über die gesamte Eingabe gesammelte Daten ausgegeben
   * werden */
  virtual void finalize();

protected:
  Base(ConstructParam&);
  Base(const Base&) = delete;
  Base &operator=(const Base&) = delete;

  utf::Writer &out;
};

class Factory;

class Registry {
public:

  /* Stelle eine Liste der Aufrufskürzel aller registrierten
   * Methoden zusammen, zur Anzeige an die Benutzer */
  static std::vector<std::u32string> getMethodList();

  /* Gibt die Beschreibung (Hilfstext, Parameterbeschreibungen)
   * für die Methode mit Aufrufkürzel 'name' zurück */
  static const Desc
  &getMethodDesc(const std::u32string &name);

  /* Prüfe, ob die Methode mit Aufrufkürzel 'name' existiert */
  static bool hasMethod(const std::u32string &name);

  /* Instanziere eine Methode mit dem Aufrufkürzel 'name' */
  static Base::Handle
  constructMethod(const std::u32string &name,
                  Base::ConstructParam &param);

private:
  friend class Factory;
  static void registerMethod(const Factory&, const std::u32string &);
  static void unregisterMethod(const std::u32string &);
};

}
}

#endif // METHOD_REGISTRY_H
