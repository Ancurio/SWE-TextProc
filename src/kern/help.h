#ifndef HELP_H
#define HELP_H

#include <string>
#include "parameter.h"

namespace tep {
namespace help {

extern const std::u32string helpCom;
extern const std::u32string testCom;

/* Ausgabe, wenn tep ohne Parameter aufgerufen wird */
void generalUsage();

/* Ausgabe, wenn 'hilfe' ohne Parameter aufgerufen wird */
void commandUsage();

/* Ausgabe, wenn 'hilfe' mit Parameter aufgerufen wird */
void commandUsage(const std::u32string &com);

/* Gebe nur die vorhandenen Befehle aus */
void printCommandList();

/* Gebe nur die Parameterhilfe für eine Methode aus */
void methodParamUsage(const std::vector<param::Desc> &paramDesc);

/* Hilfsausgabe für 'teste' */
void testUsage();

}
}

#endif // HELP_H
