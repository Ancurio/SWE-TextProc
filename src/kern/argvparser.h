#ifndef ARGVPARSER_H
#define ARGVPARSER_H

#include <string>
#include <vector>
#include "parameter.h"

namespace tep {
namespace argv {

using namespace param;

/* Diese Klasse ist noch in stub-Form und wird später mit
 * entsprechenden Fehlercodes erweitert. Sie soll aber schon
 * an den richtigen Stellen geworfen werden */
class Error : public std::runtime_error
{
public:
  Error(const std::u32string &what);
};

/* Parst die übergebenen Parameterstrings ('args') anhand
 * der übergebenen Parameterbeschreibungen ('desc'),
 * und schreibt die geparsten Werte in 'values' rein.
 * Sind die Parameter ungültig (falscher Typ, zu wenig etc.),
 * wird ein 'Error' geworfen */
void parse(const std::vector<std::u32string> &args,
           const std::vector<Desc> &desc,
           std::vector<Value> &values);

}
}

#endif // ARGVPARSER_H
