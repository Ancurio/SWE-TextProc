#include <fstream>
#include <stdexcept>
#include "io.h"
#include "utf.h"

namespace tep {
namespace io {

using namespace tep;

std::vector<std::u32string>
readListFile(const std::u32string &filename) {
  std::fstream fs(utf::convert(filename), std::ios_base::in);

  if (!fs.is_open()) {
    std::string errStr;
    errStr.append("Datei '");
    errStr.append(utf::convert(filename));
    errStr.append("' kann nicht geöffnet werden");

    throw std::runtime_error(errStr);
  }

  std::vector<std::u32string> result;
  std::string line;

  while (std::getline(fs, line)) {
    if (line.empty()) {
      /* Ignoriere leere Zeilen */
      continue;
    }

    try {
      result.push_back(utf::convert(line));
    } catch (const std::range_error&) {
      /* Kein valides UTF-8, ignoriere Zeile */
    }
  }

  return result;
}

StdErr::StdErr(std::ostream &stream)
  : stream(stream) {
}

static void strReplace(std::string &str,
                       const std::string &what,
                       const std::string &with) {
  size_t pos;

  while ((pos = str.find(what)) != std::string::npos) {
    str.replace(pos, with.size(), with);
  }
}

/* cmd.exe kann mit UTF-8 Ausgabe nicht umgehen */
static void escapeGermanChars(std::string &str) {
  strReplace(str, "ä", "ae");
  strReplace(str, "ö", "oe");
  strReplace(str, "ü", "ue");
  strReplace(str, "Ä", "Ae");
  strReplace(str, "Ö", "Oe");
  strReplace(str, "Ü", "Ue");
  strReplace(str, "ß", "ss");
}

StdErr &StdErr::operator<<(Constant k) {
  return (*this << '\n');
}

StdErr &StdErr::operator<<(const std::string &str) {
  std::string tmp = str;

#ifdef TEP_TARGET_WIN32
  escapeGermanChars(tmp);
#endif

  stream << tmp;
  return *this;
}

StdErr &StdErr::operator<<(const std::u32string &str) {
  std::string u8 = utf::convert(str);

#ifdef TEP_TARGET_WIN32
  escapeGermanChars(u8);
#endif

  stream << u8;
  return *this;
}

StdErr &StdErr::operator<<(char32_t ch) {
  static std::u32string tmp(1, U'\0');
  tmp.front() = ch;

  return (*this << tmp);
}

StdErr cerr(std::cerr);

Debug::Debug() {
  /* Sorge dafür, dass true/false auch in dieser
   * Form ausgegeben werden und nicht als 0/1 */
  buf << std::boolalpha;
}

Debug::~Debug() {
  /* Der Destruktor wird am Ende des Streamausdruckes
   * aufgerufen und sorgt für die Ausgabe samt newline */
  io::cerr << buf.str() << io::endl;
}

Debug &Debug::noSpaces() {
  spaces = false;
  return *this;
}

/* Nur die Unicode Stringklassen können nicht direkt an
 * die STL Streams geschrieben werden; hier ist eine
 * Konvertierung nach UTF-8 nötig */
Debug &Debug::operator<<(const std::u32string &str) {
  return (*this << utf::convert(str));
}

Debug &Debug::operator<<(const char32_t &str) {
  return (*this << utf::convert(std::u32string(1,str)));
}

}
}
