#include <cstdint>
#include <cstddef>
#include <array>

namespace tep {

static constexpr size_t hyphBitsetMinLen = 1;
static constexpr size_t hyphBitsetMaxLen = 17;

static constexpr size_t hyphBitset1Size = 32768;
extern const std::array<uint8_t, 32768> hyphBitset1;
static constexpr size_t hyphBitset2Size = 32768;
extern const std::array<uint8_t, 32768> hyphBitset2;

}
