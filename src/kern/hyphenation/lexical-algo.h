#ifndef LEXICAL_ALGO_H
#define LEXICAL_ALGO_H

#include <string>
#include <vector>
#include "hyphenation.h"

namespace tep {
namespace hyph {

HyphList lexicalSplit(const std::u32string &word,
                      bool debug = false);

}
}

#endif // LEXICAL_ALGO_H
