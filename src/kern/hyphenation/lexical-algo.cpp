#include "hyphenation.h"
#include "hyph-bitset.h"
#include "hashset.h"
#include "util.h"
#include "io.h"

/* Dieser "lexikografische" Algorithmus zur deutschen Worttrennung
 * stützt sich zum Großteil auf den vorhandenen Datensatz korrekter
 * Worttrennungen aus dem Wiktionary.
 * Dazu wurden alle möglichen Wortteile (Silben), die in deutschen
 * Worttrennungen vorkommen, gesammelt.
 * Ein Wort wird rekursiv durchlaufen, und jede Trennung, in der
 * alle Wortteile gültig, d.h. schon einmal in einer richtigen
 * Worttrennung vorkamen, gesammelt.
 * Schließlich wird jeder dieser Kandidaten heuristisch bewertet,
 * und die am besten bewertete Trennung als Lösung zurückgegeben.
 */

namespace tep {
namespace hyph {

typedef std::u32string::const_iterator CI;

/* Prüfe, ob 'ch' ein deutscher Vokal ist */
static bool isVowel(char32_t ch) {
  switch (ch) {
  case U'a':
  case U'e':
  case U'i':
  case U'o':
  case U'u':
  case U'y':
  case U'ä':
  case U'ö':
  case U'ü':
    return true;
  default:
    return false;
  }
}

/* Prüfe, ob 'str' nur aus deutschen Konsonanten besteht */
static bool onlyConsonants(const std::u32string &str) {
  for (char32_t ch : str) {
    if (isVowel(ch)) {
      return false;
    }
  }

  return true;
}

/* Ermittle eine Bewertung für einen Kandidaten der
 * Trennung eines Wortes.
 * Die angewandten Heuristiken stützen sich
 * auf empirische Ergebnisse meinerseits */
static int32_t rankHyphenation(const HyphList &hyph) {

  /* Grundsätzlich ist eine feinere Trennung zu
   * bevorzugen (vgl. "zin|ken" und "zinken") */
  int32_t rank = hyph.size();

  char32_t lastEnd = U'\0';

  for (const std::u32string &part : hyph) {
    /* Je mehr Wortteile mit Konsonanten beginnen,
     * desto besser (vgl. "Kan|ten" und "Kant|en") */
    if (isVowel(part.front())) {
      rank -= 3;
    } else {
      rank += 2;
    }

    /* Je mehr Wortteile mit Konsonanten enden,
     * desto besser */
    if (part.size() >= 2 && !isVowel(part.back())) {
      rank += 1;
    }

    /* Versuche, Trennungen der folgenden Konsonanten-
     * kombinationen zu vermeiden: */

    /* ch -> c|h */
    if (lastEnd == U'c' && part.front() == U'h') {
      rank -= 8;
    }

    /* tr -> t|r */
    if (lastEnd == U't' && part.front() == U'r') {
      rank -= 4;
    }

    /* pl -> p|l */
    if (lastEnd == U'p' && part.front() == U'l') {
      rank -= 4;
    }

    /* sch -> s|ch */
    if (lastEnd == U's' && part.size() >= 2 &&
        part[0] == U'c' && part[1] == U'h') {
      rank -= 8;
    }

    lastEnd = part.back();
  }

  return rank;
}

/* Prüfe, ob 'str' im bitset liegt.
 * Das Ergebnis kann nur als notwendige, nicht
 * aber als hinreichende Bedingung gewertet werden,
 * sprich: bei false ist 'str' definitiv nicht im
 * bitset, aber true garantiert nicht, dass es
 * im bitset liegt */
template<class Array>
static bool checkHashSet(const std::u32string &str,
                         const Array &bitset,
                         hashset::HashFunc hashFunc) {
  uint8_t mask;
  const uint8_t &byte =
    hashset::getBit(str, bitset, hashFunc, mask);

  return (byte & mask);
}

/* Prüfe mittels Heuristiken, ob 'str' ein
 * mögliches Wortteil (Silbe) in der Worttrennung
 * der deutschen Sprache darstellt.
 * Es werden zwei Bitsets mit unterschiedlichen
 * Hashfunktionen verwendet, um die Rate von falschen
 * Positiven so gering wie möglich zu halten. */
static bool isHyph(const std::u32string &str) {
  if (str.empty()) {
    return false;
  }

  if (onlyConsonants(str)) {
    return false;
  }

  return checkHashSet(str, hyphBitset1, hashset::hash1) &&
         checkHashSet(str, hyphBitset2, hashset::hash2);
}

/* Daten, die jeder Rekursionsaufruf benötigt */
struct RecurseData {
  const int maxDepth;
  const CI wordEnd;
  HyphList bestResult;
  int32_t bestRank;
  bool debug;
};

/* Wandle eine Worttrennung in einen mit Pipesymbol
 * se|pa|rier|ten String um */
static std::u32string
toString(const HyphList &hyphList) {
  std::u32string str;
  for (const std::u32string &part : hyphList) {
    str.append(part);
    str.append(1, U'|');
  }

  str.pop_back();

  return str;
}

/* Versuche rekursiv, eine mögliche Worttrennung zu finden.
 * 'pos' gibt die aktuelle Position im String an, ab der das
 * nächste Wortteil gesucht wird. */
static void
recurse(int curDepth, CI pos, HyphList curResult, RecurseData &data) {
  if (curDepth >= data.maxDepth) {
    return;
  }

  /* Puffer für aktuelles Wortteil */
  std::u32string syl;

  for (; pos != data.wordEnd; ++pos) {
    syl.push_back(*pos);

    if (syl.size() > hyphBitsetMaxLen) {
      /* Wenn der Kandidat bereits größer als jedes
       * mögliche Wortteil ist, hat es keinen Sinn mehr
       * weiterzusuchen. Breche diesen Ast ab. */
      return;
    }

    if (!isHyph(syl)) {
      /* Wenn der Kandidat kein mögliches Wortteil darstellt,
       * nehme einen weiteren Buchstaben dazu und versuche
       * es erneut. */
      continue;
    }

    /* Gültiges Wortteil gefunden. Speichere es im aktuellen
     * Ergebnispuffer und starte einen neuen Suchast ab hier. */
    curResult.push_back(syl);
    recurse(curDepth + 1, pos+1, curResult, data);
    curResult.pop_back();
  }

  if (!isHyph(syl)) {
    /* Übriger Restteil war kein gültiges Wortteil.
     * => fail Ast */
    return;
  }

  /* Das Wort ist komplett durchlaufen, und alle
   * ermittelten Wortteile der Trennung sind
   * gültige Kandidaten. */
  curResult.push_back(syl);
  int32_t rank = rankHyphenation(curResult);

  if (data.debug) {
    io::Debug() << toString(curResult) << rank;
  }

  /* Haben wir eine Trennung mit besserer Bewertung als
   * das bisherige Ergebnis gefunden, so speicher es als
   * neues bestes Ergebnis. */
  if (rank > data.bestRank) {
    data.bestRank = rank;
    data.bestResult.swap(curResult);
  }
}

HyphList lexicalSplit(const std::u32string &word, bool debug) {
  const HyphList defResult{ word };

  if (word.size() <= 2) {
    return defResult;
  }

  /* Die komplette Trennung läuft über Kleinbuchstaben */
  std::u32string lower = util::toGermanLower(word);

  /* Starte Rekursion */
  RecurseData data{ 16, lower.cend(), defResult, INT32_MIN, debug };
  recurse(0, lower.cbegin(), HyphList(), data);

  /* Stelle die ursprüngliche Kapitalisierung wieder her */
  HyphList::iterator pIter = data.bestResult.begin();
  std::u32string::iterator cIter = pIter->begin();

  for (char32_t ch : word) {
    *cIter++ = ch;

    if (cIter == pIter->end()) {
      ++pIter;
      cIter = pIter->begin();
    }
  }

  return data.bestResult;
}

}
}
