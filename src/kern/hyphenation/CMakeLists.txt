add_subsources (
  lexical-algo.cpp
  hyph-bitset.cpp
)

add_subheaders (
  lexical-algo.h
  hyph-bitset.h
)

add_subinclude ()
