
/* mingw dirent.h ist kaputt */
#ifdef TEP_TARGET_WIN32
struct _finddata_t {
};
#endif

#include <dirent.h>
#include "directory.h"
#include "utf.h"
#include "defines.h"

namespace tep {
namespace directory {

static void recurse(std::vector<std::string> &list,
                    DIR *cur, const std::string &name) {
  DIR *dir;
  struct dirent *entry;

  if (!(entry = ::readdir(cur))) {
    return;
  }

  do {
    std::string dname = entry->d_name;

    if (dname == "." || dname == "..") {
      continue;
    }

    std::string path = name + PATH_DELIM + dname;

    dir = ::opendir(path.c_str());

    if (!dir) {
      list.push_back(path);
      continue;
    }

    recurse(list, dir, path);
    closedir(dir);

  } while ((entry = readdir(cur)));
}

std::vector<std::string> collectFileList(const std::string &root) {
  std::vector<std::string> list;

  DIR *dir = ::opendir(root.c_str());

  if (!dir) {
    return list;
  }

  recurse(list, dir, root);
  ::closedir(dir);

  return list;
}

}
}
