#ifndef PATTERN_H
#define PATTERN_H

#include <string>
#include <vector>
#include <memory>

namespace tep {
namespace regex {

/* Lediglich das Interface ist öffentlich,
 * damit die komplette boost::xpressive Einbindung
 * in einer einzigen Objektdatei instanziiert wird.
 * Das reduziert die Rekompilationszeiten deutlich. */
class Pattern
{
public:
  typedef std::unique_ptr<Pattern> Handle;

	struct Range {
    /* Index des ersten Zeichens in der erkannten Zeichenkette */
    std::u32string::size_type begin;
    /* Index des ersten Zeichens nach der erkannten Zeichenkette */
    std::u32string::size_type end;
	};

  virtual ~Pattern();

  /* Suche nach Mustervorkomnissen in 'str' */
	virtual std::vector<Range> findMatches(const std::u32string &str) = 0;

  /* Tausche interne Resourcen mit 'o' */
  virtual void swap(Pattern &o) = 0;

  /* Erzeuge eine Pattern-Instanz.
   * Wirft std::exception bei fehlerhaftem Muster */
  static Handle construct(const std::u32string &desc);

  /* Für Lua bindings */
  static size_t implSize();
  static void implConstruct(void *p);

protected:
  Pattern();
	Pattern(const Pattern&) = delete;
	Pattern &operator=(const Pattern&) = delete;
};

}
}

#endif // PATTERN_H
