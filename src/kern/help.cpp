#include <vector>
#include <cassert>
#include "help.h"
#include "io.h"
#include "method-registry.h"
#include "util.h"

namespace tep {
namespace help {

using namespace io;
using namespace param;

static const std::string generalStr =
  "Nutzung: tep UNTERBEFEHL [PARAMETER]...";

static const std::string helpUsageStr =
  "Für detailierte Hilfe:\n"
  "  tep hilfe UNTERBEFEHL";

const std::u32string helpCom = U"hilfe";
const std::u32string testCom = U"teste";

static void methodUsage(const std::u32string &method);

void generalUsage() {
  Debug() << generalStr;
  Debug().noSpaces() << "Nutzen Sie 'tep "
                      << helpCom <<"' für mehr Informationen.";
}

static void helpUsage() {
  Debug() << helpUsageStr;
}

void commandUsage() {
  Debug() << generalStr;
  Debug() << "";
  helpUsage();
  Debug() << "";

  printCommandList();
}

void commandUsage(const std::u32string &com) {
  if (com == helpCom) {
    helpUsage();
    return;
  }

  if (com == testCom) {
    testUsage();
    return;
  }

  if (!method::Registry::hasMethod(com)) {
    Debug().noSpaces() << "Der Befehl '" << com << "' existiert nicht.";
    printCommandList();

    return;
  }

  methodUsage(com);
}

static std::vector<std::u32string>
getCommandList() {
  std::vector<std::u32string> result;

  result.push_back(helpCom);
  result.push_back(testCom);

  std::vector<std::u32string> mList =
    method::Registry::getMethodList();

  result.insert(result.end(), mList.cbegin(), mList.cend());

  return result;
}

void printCommandList() {
  Debug() << "Verfügbare Unterbefehle:";

  std::vector<std::u32string> coms = getCommandList();

  for (const std::u32string &c : coms) {
    Debug().noSpaces() << "  " << c;
  }
}

static std::u32string
valueExample(const param::Desc &d) {
  switch (d.type) {
  case param::String:
    return U"\"Stern\"";

  case param::Character:
    return U"\":\"";

  case param::Pattern:
    return U"\"[[:alpha:]]+\"";

  case param::Integer:
    return U"13";

  case param::IntList:
    return U"3,-1,4/6";

  case param::Enum:
    return d.validEnums.front();

  case param::Undefined:
    assert(!"unreachable");
    return std::u32string();
  }
}

static void usageExample(const std::u32string &method,
                         const std::vector<param::Desc> &desc) {
  std::u32string buffer(U"$ tep ");

  buffer.append(method);

  for (const param::Desc &d : desc) {
    buffer.push_back(U' ');
    buffer.append(valueExample(d));
  }

  Debug() << buffer;
}

static void methodUsage(const std::u32string &method) {
  const method::Desc &methDesc =
    method::Registry::getMethodDesc(method);

  Debug() << "Beschreibung:";
  Debug() << methDesc.help;
  Debug() << "";

  methodParamUsage(methDesc.param);
  Debug() << "";

  Debug() << "Aufrufbeispiel:";
  usageExample(method, methDesc.param);
}

void methodParamUsage(const std::vector<param::Desc> &paramDesc) {
  Debug() << "Parameter:";

  for (size_t k = 0; k < paramDesc.size(); ++k) {
    const Desc &d = paramDesc[k];

    if (k > 0) {
      Debug() << "";
    }

    Debug().noSpaces() << "  --" << d.name;

    auto helpTextLines = util::split(d.helpText, U'\n');

    for (const auto &l : helpTextLines) {
      Debug().noSpaces() << "  " << l;
    }

    /* Gebe bei Enums auch die Menge an gültigen Werten mit aus */
    if (d.type == Enum) {
      std::u32string validVals;

      for (size_t i = 0; i < d.validEnums.size(); ++i) {
        if (i > 0) {
          validVals.append(U", ");
        }

        validVals.append(U'"' + d.validEnums[i] + U'"');
      }

      Debug().noSpaces() << "  Möglichkeiten: " << validVals;
    }
  }
}

void testUsage() {
  Debug() << "Nutzung: tep teste DATEI.LUA [PARAMETER]...";
  Debug() << "";
  Debug() << "Die Erweiterung DATEI.LUA wird dadurch ausgeführt,";
  Debug() << "ohne dass sie installiert sein muss.";
}

}
}
