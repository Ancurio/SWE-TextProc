#ifndef IO_H
#define IO_H

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

namespace tep {
namespace io {

/* Lese die Datei mit Pfad 'filename' ein und
 * gebe alle darin enthaltenen Zeilen zurück */
std::vector<std::u32string>
readListFile(const std::u32string &filename);

enum Constant {
  endl
};

/* Wrapperklasse um std::cerr, die std::u32string / char32_t
 * akzeptiert und auf Windows automatisch Umlaute umwandelt
 * ('ä' -> 'ae' usw.) */
class StdErr {
public:
  StdErr(std::ostream &stream);

  template<typename T>
  StdErr &operator<<(const T &value) {
    stream << value;
    return *this;
  }

  StdErr &operator<<(Constant k);
  StdErr &operator<<(const std::string &str);
  StdErr &operator<<(const std::u32string &str);
  StdErr &operator<<(char32_t ch);

private:
  std::ostream &stream;
};

/* Einfach statt std::cerr io::cerr und statt
 * std::endl io::endl verwenden */
extern StdErr cerr;

/* Hilfsklasse für das Schreiben von Text auf die
 * Diagnoseausgabe (stderr).
 * Die Klasse fügt automatisch Leerzeichen zwischen alle
 * gestreamten Objekte und einen Zeilenumbruch am Ende ein.
 *
 * Verwendung:
 *   Debug() << "Ein text" << "noch ein Text";
 */
class Debug {
  /* Puffer, der die auszugebende Zeile aufnimmt */
  std::stringstream buf;
  bool spaces = true;

public:
  Debug();
  ~Debug();

  /* Gebe keine Leerzeichen und Zeilenumbruch aus.
   * Verwendung:
   *   StdErr().noSpaces() << "Zeile" << "ohne" << "Leerzeichen";
   */
  Debug &noSpaces();

  template<typename T>
  Debug &operator<<(const T &val) {

    /* Eigentlich werden fast alle Typen, die man ausgeben will,
     * bereits von std::cout und Freunden unterstützt */
    buf << val;

    if (spaces) {
      buf << ' ';
    }

    return *this;
  }

  Debug &operator<<(const std::u32string &str);
  Debug &operator<<(const char32_t &ch);
};

}
}

#endif // IO_H
