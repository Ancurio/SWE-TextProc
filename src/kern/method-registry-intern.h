#ifndef METHOD_REGISTRY_INTERN_H
#define METHOD_REGISTRY_INTERN_H

#include "method-registry.h"
#include "parameter.h"

namespace tep {
namespace method {

/* Die Factory ist für die Instanzierung von Methoden
 * verantwortlich und ein Implementationsdetail der
 * Modulregistry. */
class Factory {
protected:
  friend class Registry;

  Factory(const std::u32string &methodName);
  ~Factory();

  Factory(const Factory&) = delete;
  Factory &operator=(const Factory&) = delete;

  virtual Base::Handle construct(Base::ConstructParam &param) const = 0;
  virtual const Desc &getDesc() const = 0;

  /* Aufrufkürzel der Methode */
  const std::u32string name;
};

/* Methoden können dieses Makro verwenden um sich im Hauptprogramm
 * als verfügbar anzumelden. 'ModuleClass' ist eine von method::Base
 * abgeleitete Klasse die die Implementation der Methode beinhaltet.
 * 'METHOD_name' ist das Aufrufkürzel als String, das den Benutzern
 * auf der Kommandozeile angezeigt wird. */
#define REGISTER_METHOD(ModuleClass, method_name, methodDesc) \
  class ModuleClass##Factory : public tep::method::Factory { \
  public: \
  ModuleClass##Factory() : tep::method::Factory(method_name) {} \
    tep::method::Base::Handle construct(tep::method::Base::ConstructParam &param) const { \
      return tep::method::Base::Handle(new ModuleClass(param)); \
    } \
    const tep::method::Desc &getDesc() const { \
      return methodDesc; \
    } \
  } static const _tepModFact;

}
}

#endif // METHOD_REGISTRY_INTERN_H
