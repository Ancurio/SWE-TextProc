#include "hashset.h"

namespace tep {
namespace hashset {

#define HASH_MAGIC1 101
#define HASH_MAGIC2 0xDEAD

uint64_t hash1(const std::u32string &str) {
  uint64_t result = 0;

  for (char32_t ch : str) {
    result = result * HASH_MAGIC1 + ch;
  }

  return result;
}

uint64_t hash2(const std::u32string &str) {
  uint64_t result = 0;

  for (char32_t ch : str) {
    result = result * HASH_MAGIC2 + ch;
  }

  return result;
}



}
}
