#ifndef HYPHENATION_H
#define HYPHENATION_H

#include <string>
#include <vector>

namespace tep {

typedef std::vector<std::u32string> HyphList;

/* Teile ein Wort in Teile (meist Silben) auf, so dass dazwischen
 * ein Zeilenumbruch gesetzt werden kann */
HyphList splitWord(const std::u32string &word);

}

#endif // HYPHENATION_H
