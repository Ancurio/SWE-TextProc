#include <cassert>
#include <memory>
#include "parameter.h"
#include "regex.h"

namespace tep {
namespace param {

Value::Value()
  : type(Undefined) {
}

size_t Value::getEnumRaw() {
  assert(type == Enum);
  type = Undefined;

  return enumIndex;
}

std::u32string Value::getString() {
  assert(type == String);
  type = Undefined;

  return std::move(string);
}

char32_t Value::getCharacter() {
  assert(type == Character);
  type = Undefined;

  return character;
}

int Value::getInteger() {
  assert(type == Integer);
  type = Undefined;

  return integer;
}

std::vector<int> Value::getIntList() {
  assert(type == IntList);
  type = Undefined;

  return std::move(intList);
}

regex::Pattern::Handle Value::getPattern() {
  assert(type == Pattern);
  type = Undefined;

  return std::move(pattern);
}

void Value::setString(const std::u32string &val) {
  type = String;
  string = val;
}

void Value::setCharacter(char32_t val) {
  type = Character;
  character = val;
}

void Value::setInteger(int val) {
  type = Integer;
  integer = val;
}

void Value::setEnumIdx(size_t val) {
  type = Enum;
  enumIndex = val;
}

void Value::setIntList(const std::vector<int> &val) {
  type = IntList;
  intList = val;
}

void Value::setPattern(std::unique_ptr<regex::Pattern> val) {
  type = Pattern;
  pattern = std::move(val);
}

}
}
