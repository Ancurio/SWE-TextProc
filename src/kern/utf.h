#ifndef UTF_H
#define UTF_H

#include <string>
#include <ostream>
#include <istream>
#include <cstdint>

namespace tep {
namespace utf {

/* Konvertiere UTF-8 -> UTF-32 */
std::u32string convert(const std::string &str);

/* Konvertiere UTF-32 -> UTF-8 */
std::string convert(const std::u32string &str);

class Reader;

/* Hilfsklasse für alle Ausgabetexte,
 * zu verwenden anstatt std::cout */
class Writer {
public:
  Writer(std::ostream &stream);

  /* Gebe eine UTF-32 kodierte Zeile aus.
   * Der Zeilenumbruch wird automatisch eingefügt */
  void writeLine(const std::u32string &line);

  /* Gebe einen UTF-32 kodierten String
   * ohne Zeilenumbruch aus */
  void writeString(const std::u32string &str);

  /* Gebe ein UTF-32 kodiertes Zeichen
   * ohne Zeilenumbruch aus */
  void writeChar(char32_t ch);

  /* Gebe eine positive Ganzzahl
   * ohne Zeilenumbruch aus */
  void writeUInt(uint64_t i);

  /* Gebe eine Ganzzahl
   * ohne Zeilenumbruch aus */
  void writeInt(int64_t i);

  /* Gebe einen Zeilenumbruch manuell aus */
  void writeNewline();

private:
  friend class Reader;
  std::ostream &stream;
  std::string linefeed;
};

class Reader {
public:
  Reader(std::istream &stream,
         Writer &writer);

  bool getLine(std::u32string &str);

private:
  std::istream &stream;
  Writer &writer;
};

}
}

#endif // UTF_H
