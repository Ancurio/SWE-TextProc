#include "argvparser.h"
#include "regex.h"
#include "utf.h"
#include <iostream>
#include <stdexcept>
#include <cassert>

namespace tep {
namespace argv {

Error::Error(const std::u32string &what)
  : std::runtime_error(utf::convert(what))
{}

void split(std::vector<std::u32string> &argSplit,
           const std::u32string &arg,
           const char32_t delimiter) {
  argSplit.clear();

  /*Erstes Element einfügen, muss für for-Schleife vorhanden sein*/
  argSplit.push_back(U"");
  int j = 0;

  /*Man geht durch alle element. Solange kein Delimiter vorkommt, wird auf das
   * letzte Element gepusht. Trifft man auf einen Delimiter, so wird ein neues
   * Element erzeugt(push_back()) und der Delimiter wird übersprungen*/
  for (int i = 0; i < arg.size(); i++) {
    if (arg[i] == delimiter) {
      argSplit.push_back(U"");
      j++;
    } else {
      argSplit[j] = argSplit[j] + arg[i];
    }
  }
}

void inflate(std::vector<std::u32string> &argSplit, const char32_t delimiter) {
  std::vector<std::u32string> argInflate;

  /* Jedes Element wird bei dem Delimiter gesplitet.
   * Wenn es ein split gibt, gibt es nur 2 Elemente.
   * Somit wird inflated.*/
  for (int i = 0; i < argSplit.size(); i++) {
    std::vector<std::u32string> tmp;

    tmp.clear();
    tmp.push_back(U"");

    int first = 0;
    int last = 0;

    split(tmp,argSplit[i],delimiter);

    /*gibt es einen delimiter, somit ist tmp.size() gleich 2*/
    if (tmp.size() == 2) {

      /* Es Darf keine leeren Strings geben (aus "/2" oder "2/" entstanden
       * stoi kann keine leeren strings parsen und wirft exceptions */
      try {
        first = std::stoi(tep::utf::convert(tmp[0]));
      } catch(const std::exception&) {
          throw Error(U"\"" + tmp[0] + U"\" ist keine gültige Zahl");
      }
      try {
        last = std::stoi(tep::utf::convert(tmp[1]));
      } catch(const std::exception&) {
          throw Error(U"\"" + tmp[1] + U"\" ist keine gültige Zahl");
      }
      if (first <= last) {
        /*wenn die Erste Zahl kleiner ist,
         * als die Zweite, so muss Raufgezählt werden */
        do {
          argInflate.push_back(tep::utf::convert(std::to_string(first)));
          first ++;
        } while(first <= last);
      } else {
        /*Sonst ist die erste Zahl größer als die Zweite
         * und es muss runtergezählt werden */
        do {
          argInflate.push_back(tep::utf::convert(std::to_string(first)));
          first --;
        } while(first >= last);
      }
    } else {
      /*wenn der Split keine 2 Zahlen zurückgibt, gibt es nur eine und es muss
       * nicht inflated werden.
       * Die Zahl wird einfach in die Liste gepusht. */
      argInflate.push_back(tmp[0]);
    }
  }

  argSplit = argInflate;
}


void changeValue(Value &value,
                 const Desc &desc,
                 const std::u32string &arg) {
  switch(desc.type) {

  /*String wird übernommen*/
  case String:
    value.setString(arg);
    break;

  /*1. charakter wird übernommen, wenn mehr oder weniger als einer: error*/
  case Character:
    if(arg.size() == 1) {
      value.setCharacter(arg[0]);
    } else {
      throw Error(desc.name + U": \"" + arg + U"\" ist kein Zeichen");
    }
    break;

  /*Pattern wird übernommen und error weitergeleited*/
  case Pattern:
    try {
      value.setPattern(regex::Pattern::construct(arg));
    } catch(const std::exception &exc) {
      throw Error(desc.name + U": \"" + arg + U"\" ist kein gültiges Muster"
                  + utf::convert(exc.what()));
    }
    break;

  /*Prüfen ob es die Enum gibt, sonst wird ein Error geworfen*/
  case Enum:
    for (size_t i = 0; i < desc.validEnums.size(); ++i) {
      if (arg != desc.validEnums[i]) {
        continue;
      }

      value.setEnumIdx(i);
      return;
    }
    throw Error(desc.name + U": '" + arg +
                U"' ist kein erlaubter Wert");
    break;

  /* mit stoi die strings umwandeln.
   * stoi achtet auf das "-" und Buchstaben werden ignoriert.
   * stoi wirft errors, wenn die umwandlung nicht möglich ist */
  case Integer:
    try {
      value.setInteger(std::stoi(utf::convert(arg)));
    } catch(const std::exception&) {
      throw Error(desc.name + U": Ungültige Zahl \"" + arg + U"\"");
    }
    break;

  /* erst an allen ',' splitten.
   * dann an allen '/' splitten und mit den Zahlen, die dazwichen sind füllen.
   * dannach die liste mit stoi in int umwandeln. */
  case IntList: {
    std::vector<int> intList;
    std::vector<std::u32string> argSplit;

    split(argSplit, arg, U',');
    inflate(argSplit, U'/');

    for (int i = 0; i < argSplit.size(); i++) {
      try {
        intList.push_back(std::stoi(utf::convert(argSplit[i])));
      } catch(const std::exception&) {
        throw Error(desc.name + U": Ungültige Zahl \"" + argSplit[i] + U"\"");
      }
    }

    value.setIntList(intList);
    break;

  }

  default:
    assert(!"Invalid parameter type");
    break;
  }
}


/* Gibt die Position des gesuchten parameters zurück.
 * Wenn nicht auffindbar, gibt es -1 zurück */
int findParam(const std::vector<Desc> &desc, std::u32string arg) {
  for (int i = 0; i < desc.size(); ++i) {
    if (U"--"+desc[i].name == arg) {
      return i;
    }
  }
  return -1;
}

void parse(const std::vector<std::u32string> &args,  //Argumente
           const std::vector<Desc> &desc,//beschreib. der erwarteten Argumente
           std::vector<Value> &values)  //Die Werte, die Beschrieben werden
{
  /* schauen wieviele args ohne "--" existieren */
  int argsize = 0;
  for (int i = 0; i < args.size(); ++i) {
    if (args[i][0] == '-' && args[i][1] =='-') {
      /*do nothing*/
    }
    else {
      ++argsize;
    }
  }
  /* Zu wenig Argumente*/
  if (argsize < desc.size()) {
    throw Error(U"Zu wenig Parameter");
  }
  /*Zu viele Argumente*/
  if (argsize > desc.size()) {
    throw Error(U"Zu viele Parameter");
  }

  /*Values säubern*/
  values.clear();

  /*Grösse des Vectors angleichen*/
  values.resize(desc.size());

  /* vector um zu schauen ob die parameter schonmal beschrieben wurden*/
  std::vector<int> written;
  /* written == O*/
  written.resize(desc.size());

  /* var zur überprüfung ob man benannte variablen zuordnet */
  bool named = false;
  for (int i = 0; i < args.size(); ++i) {
    /* check ob Parameter benannt ist */
    if (args[i][0] == U'-' && args[i][1] == U'-') {
      named = true;
    }
    /*Unbenannte parameter einfügen*/
    if (!named) {
      changeValue(values[i],desc[i], args[i]);
      written[i] = 1;
    }
    /* benannten Parameter einfügen */
    if (named) {
      int where = -1;
      /*suchen, wo das Argument hingehört*/
      where = findParam(desc,args[i]);
      /* wenn der Parameter keiner Beschreibung zugeordnet werden kann */
      if (where < 0) {
        throw Error(U"Unbekannter Parameter \"" + args[i] + U"\"");
      }
      /* check ob der parameter schonmal beschrieben wurde */
      if (written[where] == 1) {
        throw Error(U"Parameter \"" + args[i] + U"\" mehrfach übergeben");
      }
      /* argumentnamen überspringen */
      ++i;
      /*wert wechseln */
      changeValue(values[where],desc[where],args[i]);
      written[where] = 1;
    }
  }


}

}
}
