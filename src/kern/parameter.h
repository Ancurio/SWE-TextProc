#ifndef PARAMETER_H
#define PARAMETER_H

#include <string>
#include <vector>
#include <memory>
#include "regex.h"

namespace tep {

namespace regex {
class Pattern;
}

namespace param {

/* Datentyp eines Parameters */
enum Type {
  Undefined,
  String,
  Character,
  Pattern,
  Enum,
  Integer,
  IntList,
};

/* Union-artiger Multityp für übergebene Argumente.
 * 'enumIndex' ist der 0-basierte Index des enums,
 * dass übergeben wurde */
class Value {
  Type type;

  std::u32string string;
  char32_t character;
  int integer;
  size_t enumIndex;
  std::vector<int> intList;
  std::unique_ptr<regex::Pattern> pattern;

public:
  Value();

  /* Diese getter übertragen den Besitz (ownership)
   * des Wertes; nach Aufruf ist type == Undefined */
  std::u32string getString();
  char32_t getCharacter();
  int getInteger();
  std::vector<int> getIntList();
  regex::Pattern::Handle getPattern();
  size_t getEnumRaw();

  template<typename E>
  E getEnum() {
    return static_cast<E>(getEnumRaw());
  }

  void setString(const std::u32string &val);
  void setCharacter(char32_t val);
  void setInteger(int val);
  void setEnumIdx(size_t val);
  void setIntList(const std::vector<int> &val);
  void setPattern(std::unique_ptr<regex::Pattern> val);
};

/* Beschreibung eines Parameters */
struct Desc {
  Type type;

  /* Parametername; nur bei Aufruf mit explizit
   * benannten Parametern (--param1 123) relevant.
   * "--" ist nicht Teil des Namens. */
  std::u32string name;

  /* Für Menschen lesbare Beschreibung */
  std::u32string helpText;

  /* Bei Enum: Alle gültigen Werte */
  std::vector<std::u32string> validEnums;
};

}
}

#endif // PARAMETER_H
