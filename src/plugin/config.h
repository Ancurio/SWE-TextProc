#ifndef CONFIG_H
#define CONFIG_H

#include <lua.hpp>
#include "parameter.h"
#include "method-registry.h"

namespace tep {
namespace plugin {

/* Die Table liegt auf dem Stack */
method::Desc parseTepConfig(lua_State *L);

void pushValue(lua_State *L,
               param::Value &v, param::Type type);

}
}

#endif // CONFIG_H
