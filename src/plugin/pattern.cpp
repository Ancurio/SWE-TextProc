#include <lua.hpp>
#include "plugin-common.h"
#include "bindings.h"
#include "regex.h"

namespace tep {
namespace plugin {

using namespace regex;

static const std::string className = "Pattern";
static RegistrySlot metaSlot;

void patternNewIntern(lua_State *L,
                      Pattern **out)
{
  Pattern *pat = static_cast<Pattern*>(lua_newuserdata(L, Pattern::implSize()));
  Pattern::implConstruct(pat);

  setInstanceMetaTable(L, metaSlot);

  if (out) {
    *out = pat;
  }
}

static int patternFromStr(lua_State *L) {
  std::u32string desc = getAnyString(L, 1);
  Pattern::Handle pat = regex::Pattern::construct(desc);
  Pattern *luaPat;

  patternNewIntern(L, &luaPat);
  pat->swap(*luaPat);

  return 1;
}

static int patternFindMatches(lua_State *L) {
  Pattern &self = getUserDataCheck<Pattern>(L, metaSlot, 1);
  std::u32string &str = getUStringSelf(L, 2);

  std::vector<regex::Pattern::Range> matches =
    self.findMatches(str);

  /* Result table */
  lua_newtable(L);
  int arrayIdx = 1;

  for (const regex::Pattern::Range &m : matches) {
    lua_newtable(L);

    lua_pushinteger(L, m.begin+1);
    lua_rawseti(L, -2, 1);

    lua_pushinteger(L, m.end);
    lua_rawseti(L, -2, 2);

    lua_rawseti(L, -2, arrayIdx++);
  }

  return 1;
}

static int patternTostring(lua_State *L) {
  Pattern &self = getUserDataCheck<Pattern>(L, metaSlot, 1);

  lua_pushstring(L, "Pattern");

  return 1;
}

static const luaL_Reg patternRegF[] = {
  { "from_str", LF<patternFromStr> },
  LUA_REG_END
};

static const luaL_Reg patternRegM[] = {
  { "find_matches", LF<patternFindMatches> },
  LUA_REG_END
};

static const luaL_Reg patternMetaF[] = {
  { META_TOSTRING, LF<patternTostring> },
  LUA_REG_END
};

void patternRegister(lua_State *L) {
  registerType<Pattern>(L, className, metaSlot,
                        patternRegF, patternRegM, patternMetaF);
}

}
}
