#include <string>
#include <lua.hpp>
#include "plugin-common.h"
#include "bindings.h"
#include "utf.h"

namespace tep {
namespace plugin {

static const std::string className = "UString";
static RegistrySlot metaSlot;

bool isUString(lua_State *L, int idx) {
  return isUserType(L, metaSlot, idx);
}

std::u32string &getUStringSelf(lua_State *L, int idx) {
  return getUserDataCheck<std::u32string>(L, metaSlot, idx);
}

static void
ustringError(const std::string &msg) {
  throw std::runtime_error(className + ": " + msg);
}

static int ustringNew(lua_State *L) {
  size_t n = luaL_checkinteger(L, 1);

  ustringNewIntern(L, n);

  /* new userdatum is already on the stack */
  return 1;
}

static int ustringFromStr(lua_State *L) {
  std::string u8str = luaL_checkstring(L, 1);
  std::u32string u32str = utf::convert(u8str);

  std::u32string *str;
  ustringNewIntern(L, 0, &str);
  u32str.swap(*str);

  return 1;
}

static int ustringDup(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);

  std::u32string *dup;
  ustringNewIntern(L, self.size(), &dup);
  self.copy(&(*dup)[0], self.size());

  /* new userdatum is already on the stack */
  return 1;
}

static size_t getIndex(const std::u32string &self,
                       lua_State *L, int idx) {
  int n = luaL_checkinteger(L, idx);

  if (n < 1 || n > self.size()) {
    ustringError("index outside valid range [1,len]");
  }

  return n-1;
}

static int ustringGet(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);
  size_t idx = getIndex(self, L, 2);

  lua_pushinteger(L, self[idx]);

  return 1;
}

static int ustringSet(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);
  size_t idx = getIndex(self, L, 2);
  char32_t ch = getChar32(L, 3);

  self[idx] = ch;

  return 0;
}

static int ustringPush(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);
  char32_t ch = getChar32(L, 2);

  self.push_back(ch);

  return 0;
}

static int ustringAppend(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);
  std::u32string str = getAnyString(L, 2);

  self.append(str);

  return 0;
}

static void getRange(const std::u32string &self,
                     lua_State *L, int idx,
                     size_t &outPos, size_t &outLen) {
  if (!lua_istable(L, idx)) {
    ustringError("range argument must be an array");
  }

  if (luaL_len(L, idx) != 2) {
    ustringError("range argument must have two values (begin, end)");
  }

  int begin, end;

  lua_rawgeti(L, idx, 1);
  lua_rawgeti(L, idx, 2);
  begin = luaL_checkinteger(L, -2);
  end = luaL_checkinteger(L, -1);
  lua_pop(L, 2);

  if (begin < 1 || begin > self.size()) {
    ustringError("begin outside valid range [1,len]");
  }

  if (end < 1 || end > self.size()) {
    ustringError("end outside valid range [1,len]");
  }

  if (begin > end) {
    ustringError("invalid range: begin > end");
  }

  outPos = begin-1;
  outLen = end-begin+1;
}

static int ustringReplace(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);
  std::u32string &str = getUStringSelf(L, 3);

  size_t pos, len;
  getRange(self, L, 2, pos, len);

  self.replace(pos, len, str);

  return 0;
}

static int ustringSubstr(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);

  size_t pos, len;
  getRange(self, L, 2, pos, len);

  std::u32string *result;
  ustringNewIntern(L, 0, &result);
  *result = self.substr(pos, len);

  return 1;
}

static int ustringLen(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);

  lua_pushinteger(L, self.length());

  return 1;
}

static int ustringTostring(lua_State *L) {
  std::u32string &self = getUStringSelf(L, 1);

  lua_pushstring(L, utf::convert(self).c_str());

  return 1;
}

static const luaL_Reg ustringRegF[] = {
  { "new",      LF<ustringNew>     },
  { "from_str", LF<ustringFromStr> },
  LUA_REG_END
};

static const luaL_Reg ustringRegM[] = {
  { "dup",     LF<ustringDup>     },
  { "get",     LF<ustringGet>     },
  { "set",     LF<ustringSet>     },
  { "push",    LF<ustringPush>    },
  { "append",  LF<ustringAppend>  },
  { "replace", LF<ustringReplace> },
  { "substr",  LF<ustringSubstr>  },
  LUA_REG_END
};

static const luaL_Reg ustringMetaF[] = {
  { META_TOSTRING, LF<ustringTostring> },
  { META_LEN,      LF<ustringLen>      },
  LUA_REG_END
};

void ustringNewIntern(lua_State *L, size_t n,
                      std::u32string **out) {
  std::u32string *str = allocUserData<std::u32string>(L);
  new (str) std::u32string(n, U'\0');

  setInstanceMetaTable(L, metaSlot);

  if (out)
    *out = str;
}

void ustringRegister(lua_State *L) {
  registerType<std::u32string>(L, className, metaSlot,
                               ustringRegF, ustringRegM, ustringMetaF);
}

}
}
