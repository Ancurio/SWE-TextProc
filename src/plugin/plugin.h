#ifndef PLUGIN_H
#define PLUGIN_H

#include <memory>
#include <string>
#include <utility>
#include <map>

#include "method-registry-intern.h"
#include "utf.h"

struct lua_State;

namespace tep {
namespace plugin {

class Module : public method::Factory {
public:
  typedef std::unique_ptr<Module> Handle;

  /* Wirft XXX bei ungültiger tep_config */
  Module(const std::string &filename,
         const std::u32string &name);

  void setWriter(utf::Writer &writer);

  const method::Desc &getDesc() const;

  method::Base::Handle
  construct(method::Base::ConstructParam &param) const;

  void callInit(std::vector<param::Value> &paramVals);
  void callProcessLine(const std::u32string &procLine);
  void callFini();

private:
  std::unique_ptr<lua_State, void(*)(lua_State*)> Lptr;
  lua_State *L;
  method::Desc desc;
  std::u32string *procLine;
};

/* Method name -> full Module path */
std::map<std::u32string, std::string> listInstalled();

}
}

#endif // PLUGIN_H
