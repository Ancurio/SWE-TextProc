#include <stdexcept>
#include "plugin-common.h"
#include "utf.h"
#include "bindings.h"

namespace tep {
namespace plugin {

#define CLASS_NAME_KEY "class_name"

void RegistrySlot::set(lua_State *L) {
  lua_pushlightuserdata(L, this);
  lua_rotate(L, -2, 1);
  lua_rawset(L, LUA_REGISTRYINDEX);
}

void RegistrySlot::get(lua_State *L) {
  lua_pushlightuserdata(L, this);
  lua_rawget(L, LUA_REGISTRYINDEX);
}

void registerInstMethodsImpl(lua_State *L,
                             const luaL_Reg *inst,
                             const luaL_Reg *meta,
                             RegistrySlot &metaSlot,
                             const std::string &name,
                             lua_CFunction gcFunction) {
  /* Actual metatable */
  lua_newtable(L);

  lua_pushstring(L, "__index");

  /* Table holding functions */
  lua_newtable(L);
  luaL_setfuncs(L, inst, 0);

  /* meta[__index] = funcTable */
  lua_rawset(L, -3);

  lua_pushstring(L, "__gc");
  lua_pushcfunction(L, gcFunction);
  lua_rawset(L, -3);

  lua_pushstring(L, CLASS_NAME_KEY);
  lua_pushstring(L, name.c_str());
  lua_rawset(L, -3);

  luaL_setfuncs(L, meta, 0);

  metaSlot.set(L);
}

void registerClassMethods(lua_State *L, const luaL_Reg *reg,
                          const std::string &name) {
  lua_getglobal(L, "Tep");
  lua_pushstring(L, name.c_str());

  lua_newtable(L);
  luaL_setfuncs(L, reg, 0);

  lua_rawset(L, -3);
  lua_pop(L, 1);
}

/* Object instance must be on top of stack */
void setInstanceMetaTable(lua_State *L, RegistrySlot &metaSlot) {
  metaSlot.get(L);
  lua_setmetatable(L, -2);
}

std::u32string getAnyString(lua_State *L, int idx) {
  if (isUString(L, idx)) {
    return getUStringSelf(L, idx);
  } else if (lua_isstring(L, idx)) {
    return utf::convert(lua_tostring(L, idx));
  } else {
    throw std::runtime_error("String expected");
  }
}

char32_t getChar32(lua_State *L, int idx) {
  if (!lua_isinteger(L, idx)) {
    throw std::runtime_error("expected char(integer)");
  }

  int n = lua_tointeger(L, idx);

  if (n < 0) {
    throw std::runtime_error("char value must be positive");
  }

  return static_cast<char32_t>(n);
}

bool isUserType(lua_State *L, RegistrySlot &metaSlot, int idx) {
  lua_getmetatable(L, idx);
  metaSlot.get(L);
  int equal = lua_rawequal(L, -1, -2);
  lua_pop(L, 2);

  return (equal == 1);
}

void checkUserType(lua_State *L, RegistrySlot &metaSlot, int idx) {
  if (isUserType(L, metaSlot, idx)) {
    return;
  }

  /* Type mismatch; raise error using the class name */
  metaSlot.get(L);
  lua_pushstring(L, CLASS_NAME_KEY);
  lua_rawget(L, -2);

  std::string msg("Type mismatch: Expected ");
  msg.append(lua_tostring(L, -1));
  throw std::runtime_error(msg);
}

}
}
