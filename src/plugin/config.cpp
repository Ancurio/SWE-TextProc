#include <map>
#include <stdexcept>
#include <assert.h>
#include "bindings.h"
#include "config.h"
#include "regex.h"
#include "utf.h"

namespace tep {
namespace plugin {

struct ConfError : public std::runtime_error {
  ConfError(const std::u32string &msg)
    : std::runtime_error(utf::convert(msg)) {
  }
};

static const std::map<std::u32string, param::Type> typeMap = {
  { U"string",  param::String    },
  { U"char",    param::Character },
  { U"pattern", param::Pattern   },
  { U"enum",    param::Enum      },
  { U"int",     param::Integer   },
  { U"intlist", param::IntList   },
};

static void
checkString(lua_State *L, const std::u32string &errorMsg) {
  if (!lua_isstring(L, -1))
    throw ConfError(errorMsg);
}

static void
luaRawGetStr(lua_State *L, const char *key) {
  lua_pushstring(L, key);
  lua_rawget(L, -2);
}

static std::u32string
popString(lua_State *L) {
  std::u32string result = utf::convert(lua_tostring(L, -1));
  lua_pop(L, 1);

  return result;
}

static std::u32string
getStringOptional(lua_State *L, const std::u32string &key) {
  luaRawGetStr(L, utf::convert(key).c_str());

  if (!lua_isnil(L, -1)) {
    checkString(L, key + U": muss String sein");
    return popString(L);
  } else {
    lua_pop(L, 1);
    return std::u32string();
  }
}

/* Param desc table ist auf dem Stack */
static param::Desc parseDesc(lua_State *L) {
  param::Desc desc;

  luaRawGetStr(L, "name");
  checkString(L, U"Parametername muss ein String sein");

  desc.name = popString(L);

  if (desc.name.empty()) {
    throw ConfError(U"Parametername darf nicht leer sein");
  }

  luaRawGetStr(L, "type");
  checkString(L, U"Parametertyp muss ein String sein");

  std::u32string type = popString(L);
  auto entry = typeMap.find(type);

  if (entry == typeMap.cend()) {
    throw ConfError(U"Ungültiger Parametertyp: " + type);
  }

  desc.type = entry->second;

  if (desc.type == param::Enum) {
    luaRawGetStr(L, "values");
    if (!lua_istable(L, -1)) {
      throw ConfError(desc.name + U": Enumwerte müssen in ein Array gefasst sein");
    }

    int n = luaL_len(L, -1);

    for (int i = 1; i < n+1; ++i) {
      lua_rawgeti(L, -1, i);
      checkString(L, desc.name + U": Jeder Enumwert muss ein String sein");
      desc.validEnums.push_back(popString(L));

      if (desc.validEnums.back().empty()) {
        throw ConfError(desc.name + U": Enumwert darf nicht leer sein");
      }
    }

    lua_pop(L, 1);

    if (desc.validEnums.empty()) {
      throw ConfError(desc.name + U": Mindestens ein Enumwert ist notwendig");
    }
  }

  desc.helpText = getStringOptional(L, U"help");

  return desc;
}

static std::vector<param::Desc>
parseParam(lua_State *L) {
  int n = luaL_len(L, -1);
  std::vector<param::Desc> result;

  for (int i = 1; i < n+1; ++i) {
    lua_rawgeti(L, -1, i);

    if (!lua_istable(L, -1)) {
      throw ConfError(U"tep_config.parameters Einträge müssen Tables sein");
    }

    result.push_back(parseDesc(L));
    lua_pop(L, 1);
  }

  return result;
}

method::Desc parseTepConfig(lua_State *L) {
  method::Desc result;
  result.help = getStringOptional(L, U"help");

  lua_pushstring(L, "parameters");
  lua_rawget(L, -2);

  if (!lua_istable(L, -1)) {
    throw ConfError(U"tep_config.parameters nicht definiert oder keine Table");
  }

  result.param = parseParam(L);
  lua_pop(L, -1);

  return result;
}

void pushValue(lua_State *L, param::Value &v, param::Type type) {
  switch (type) {
  case param::String:
    std::u32string *str;
    ustringNewIntern(L, 0, &str);
    *str = v.getString();
    break;

  case param::Character:
    lua_pushinteger(L, v.getCharacter());
    break;

  case param::Enum:
    lua_pushinteger(L, v.getEnumRaw() + 1);
    break;

  case param::Integer:
    lua_pushinteger(L, v.getInteger());
    break;

  case param::Pattern: {
      regex::Pattern::Handle pat = v.getPattern();
      regex::Pattern *luaPat;
      patternNewIntern(L, &luaPat);
      pat->swap(*luaPat);
      break;
    }

  case param::IntList: {
      std::vector<int> list = v.getIntList();
      lua_createtable(L, list.size(), 0);

      for (int i = 0; i < list.size(); ++i) {
        lua_pushinteger(L, list.at(i));
        lua_rawseti(L, -2, i+1);
      }

    }

    break;

  default:
    assert(!"Undefined value type");
  }
}

}
}
