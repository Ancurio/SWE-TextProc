#include <exception>
#include <lua.hpp>
#include "plugin-common.h"
#include "bindings.h"
#include "util.h"
#include "io.h"
#include "hyphenation.h"

using namespace tep;
using namespace plugin;

/* Consumes the list */
static void
createStringList(lua_State *L,
                 std::vector<std::u32string> &list) {
  lua_newtable(L);
  int arrayIdx = 1;

  for (std::u32string &line : list) {
    std::u32string *luaObj;

    ustringNewIntern(L, 0, &luaObj);
    luaObj->swap(line);

    lua_rawseti(L, -2, arrayIdx++);
  }
}

static int
utilReadListFile(lua_State *L) {
  std::u32string filename = getAnyString(L, 1);

  std::vector<std::u32string> lines = io::readListFile(filename);
  createStringList(L, lines);

  return 1;
}

static int
utilSplitWord(lua_State *L) {
  std::u32string word = getAnyString(L, 1);

  std::vector<std::u32string> hyph = splitWord(word);
  createStringList(L, hyph);

  return 1;
}

static int
utilSplitString(lua_State *L) {
  std::u32string word = getAnyString(L, 1);
  char32_t delim = getChar32(L, 2);

  std::vector<std::u32string> parts = util::split(word, delim);
  createStringList(L, parts);

  return 1;
}

static int
utilDebug(lua_State *L) {
  int narg = lua_gettop(L);
  const char *str;

  for (int i = 1; i < narg+1; ++i) {
    if (!(str = luaL_tolstring(L, i, 0))) {
      continue;
    }

    io::cerr << std::string(str) << ' ';
  }

  io::cerr << io::endl;
  return 0;
}

static const luaL_Reg utilF[] = {
  { "read_list_file", LF<utilReadListFile> },
  { "split_word",     LF<utilSplitWord>    },
  { "split_string",   LF<utilSplitString>  },
  { "debug",          LF<utilDebug>        },
  LUA_REG_END
};

void tep::plugin::utilRegister(lua_State *L) {
  lua_getglobal(L, "Tep");
  luaL_setfuncs(L, utilF, 0);
  lua_pop(L, 1);
}
