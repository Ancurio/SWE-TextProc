#ifndef BINDINGS_H
#define BINDINGS_H

#include <string>
#include <lua.hpp>
#include "regex.h"
#include "utf.h"

namespace tep {
namespace plugin {

void ustringRegister(lua_State *L);
void patternRegister(lua_State *L);
void utilRegister(lua_State *L);
void writerRegister(lua_State *L);

void ustringNewIntern(lua_State *L, size_t n,
                      std::u32string **out = nullptr);
void patternNewIntern(lua_State *L,
                      regex::Pattern **out = nullptr);

bool isUString(lua_State *L, int idx);
std::u32string &getUStringSelf(lua_State *L, int idx);

void setUtfWriter(lua_State *L, utf::Writer &w);

}
}

#endif // BINDINGS_H
