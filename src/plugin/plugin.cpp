#include <lua.hpp>
#include <array>
#include <stdexcept>
#include <cassert>
#include "plugin.h"
#include "plugin-common.h"
#include "bindings.h"
#include "io.h"
#include "config.h"
#include "directory.h"
#include "defines.h"

namespace tep {
namespace plugin {

static void
initBindings(lua_State *L) {
  luaopen_base(L);

  luaopen_table(L);
  lua_setglobal(L, "table");

  lua_newtable(L);
  lua_setglobal(L, "Tep");

  ustringRegister(L);
  patternRegister(L);
  utilRegister(L);
  writerRegister(L);
}

class MethodWrapper : public method::Base {
public:
  MethodWrapper(Module &mod, method::Base::ConstructParam &p)
    : method::Base(p), mod(mod) {
    mod.setWriter(p.out);
    mod.callInit(p.param);
  }

  void processLine(const std::u32string &line) {
    mod.callProcessLine(line);
  }

  void finalize() {
    mod.callFini();
  }

  // XXX unsafe
  Module &mod;
};

/* Stack setup zur Laufzeit:
 * -4 Ustring line
 * -3 tep_init
 * -2 tep_process_line
 * -1 tep_fini
 */
Module::Module(const std::string &filename, const std::u32string &name)
  : method::Factory(name),
    Lptr(luaL_newstate(), lua_close),
    L(Lptr.get()) {

  initBindings(L);

  if (luaL_loadfile(L, filename.c_str()) || lua_pcall(L, 0, 0, 0)) {
    throw std::runtime_error(std::string("Error loading plugin: ") + lua_tostring(L, -1));
  }

  lua_getglobal(L, "tep_config");

  if (!lua_istable(L, -1)) {
    throw std::runtime_error("tep_config nicht definiert oder keine Table");
  }

  desc = parseTepConfig(L);
  lua_pop(L, 1);

  ustringNewIntern(L, 0, &procLine);

  const std::array<std::string, 3> funcs = {
    "tep_init",
    "tep_process_line",
    "tep_fini"
  };

  for (const std::string &f : funcs) {
    lua_getglobal(L, f.c_str());

    if (!lua_isfunction(L, -1)) {
      throw std::runtime_error(f + " nicht definiert");
    }
  }
}

void Module::setWriter(utf::Writer &writer) {
  setUtfWriter(L, writer);
}

const method::Desc &Module::getDesc() const {
  return desc;
}

method::Base::Handle
Module::construct(method::Base::ConstructParam &param) const {
  Module *self = const_cast<Module*>(this);
  return method::Base::Handle(new MethodWrapper(*self, param));
}

void Module::callInit(std::vector<param::Value> &paramVals) {
  assert(paramVals.size() == desc.param.size());

  /* Push tep_init() */
  lua_pushvalue(L, -3);

  /* Create parameter table */
  lua_createtable(L, 0, desc.param.size());

  for (size_t i = 0; i < desc.param.size(); ++i) {
    lua_pushstring(L, utf::convert(desc.param[i].name).c_str());
    pushValue(L, paramVals[i], desc.param[i].type);
    lua_rawset(L, -3);
  }

  if (lua_pcallk(L, 1, 0, 0, 0, 0) != 0) {
    throw std::runtime_error(std::string("tep_init: ") + lua_tostring(L, -1));
  }
}

void Module::callProcessLine(const std::u32string &line) {
  *procLine = line;

  /* Push tep_process_line() */
  lua_pushvalue(L, -2);

  /* Push 'line' */
  lua_pushvalue(L, -4 -1);

  if (lua_pcallk(L, 1, 0, 0, 0, 0) != 0) {
    throw std::runtime_error(std::string("tep_process_line: ") + lua_tostring(L, -1));
  }
}

void Module::callFini() {
  /* Push tep_fini() */
  lua_pushvalue(L, -1);

  if (lua_pcallk(L, 0, 0, 0, 0, 0) != 0) {
    throw std::runtime_error(std::string("tep_fini: ") + lua_tostring(L, -1));
  }
}

std::map<std::u32string, std::string> listInstalled() {
  std::map<std::u32string, std::string> result;
  std::vector<std::string> files
    = directory::collectFileList(PLUGIN_PATH);

  for (std::string &f : files) {
    /* f.size() means invalid */
    size_t dotIdx = f.size();
    size_t dlmIdx = f.size();

    for (size_t i = f.size(); i > 0; --i) {
      if (f[i-1] == PATH_DELIM) {
        dlmIdx = i-1;
        break;
      }
    }

    if (dlmIdx == f.size()) {
      continue;
    }

    for (size_t i = f.size(); i > 0 && f[i-1] != PATH_DELIM; --i) {
      if (f[i-1] == '.') {
        dotIdx = i-1;
        break;
      }
    }

    /* No dot found before first slash */
    if (dotIdx == dlmIdx) {
      continue;
    }

    /* No dot found at all */
    if (dotIdx == f.size()) {
      continue;
    }

    std::string ext = f.substr(dotIdx, f.size()-dotIdx);

    /* Not a *.lua file */
    if (ext != ".lua") {
      continue;
    }

    std::u32string name = utf::convert(f.substr(dlmIdx+1, dotIdx-dlmIdx-1));
    result.insert(std::make_pair(std::move(name), std::move(f)));
  }

  return result;
}

}
}
