#ifndef PLUGINCOMMON_H
#define PLUGINCOMMON_H

#include <lua.hpp>
#include <string>
#include <exception>

namespace tep {
namespace plugin {

/* Represents a unique table entry in the registry.
 * Instances of this struct MUST be statically allocated. */
struct RegistrySlot {

  /* Pops the stack top and moves it into the slot */
  void set(lua_State *L);

  /* Pushes the slot contents onto the stack */
  void get(lua_State *L);
};

#define META_TOSTRING "__tostring"
#define META_LEN      "__len"
#define LUA_REG_END { nullptr, nullptr }

void
registerInstMethodsImpl(lua_State *L,
                        const luaL_Reg *inst,
                        const luaL_Reg *meta,
                        RegistrySlot &metaSlot,
                        const std::string &name,
                        lua_CFunction gcFunction);

void registerClassMethods(lua_State *L, const luaL_Reg *reg,
                                 const std::string &name);

void registerType(lua_State *L,
                  const std::string &name, RegistrySlot &metaSlot,
                  const luaL_Reg *classMeth,
                  const luaL_Reg *instMeth);

/* Object instance must be on top of stack */
void setInstanceMetaTable(lua_State *L, RegistrySlot &metaSlot);

std::u32string getAnyString(lua_State *L, int idx);

char32_t getChar32(lua_State *L, int idx);

bool isUserType(lua_State *L, RegistrySlot &metaSlot, int idx = -1);
void checkUserType(lua_State *L, RegistrySlot &metaSlot, int idx = -1);

template<typename T>
T &getUserData(lua_State *L, int idx = -1) {
  return *static_cast<T*>(lua_touserdata(L, idx));
}

/* Careful: Only call this function before any stack
 * objects are constructed, as it will perform a longjmp
 * on type mismatch */
template<typename T>
T &getUserDataCheck(lua_State *L, RegistrySlot &metaSlot, int idx = -1) {
  checkUserType(L, metaSlot, idx);
  return getUserData<T>(L, idx);
}

template<typename T>
T *allocUserData(lua_State *L) {
  T *obj = static_cast<T*>(lua_newuserdata(L, sizeof(T)));
  return obj;
}

template<typename T>
int gcUserData(lua_State *L) {
  T &obj = getUserData<T>(L, 1);
  obj.~T();

  return 0;
}

template<typename T>
void registerInstMethods(lua_State *L,
                         const luaL_Reg *inst, const luaL_Reg *meta,
                         RegistrySlot &metaSlot,
                         const std::string &name) {
  registerInstMethodsImpl(L, inst, meta, metaSlot, name, gcUserData<T>);
}

template<typename T>
void registerType(lua_State *L,
                  const std::string &name, RegistrySlot &metaSlot,
                  const luaL_Reg *classMeth,
                  const luaL_Reg *instMeth,
                  const luaL_Reg *metaMeth) {
  registerClassMethods(L, classMeth, name);
  registerInstMethods<T>(L, instMeth, metaMeth, metaSlot, name);
}

/* A wrapper around lua_CFunction that catches any
 * std::exceptions and safely fowards them to Lua */
template<lua_CFunction func>
int LF(lua_State *L) {
  /* This is static so we can safely longjmp out of
   * this function (no destructor needed) */
  static std::string error;

  try {
    return func(L);
  } catch (const std::exception &exc) {
    error = exc.what();
  }

  lua_pushstring(L, error.c_str());
  lua_error(L);

  return 0;
}

}
}

#endif // PLUGINCOMMON_H
