#include "plugin-common.h"
#include "utf.h"

namespace tep {
namespace plugin {

static RegistrySlot writerSlot;

void setUtfWriter(lua_State *L, utf::Writer &w) {
  lua_pushlightuserdata(L, &w);
  writerSlot.set(L);
}

static utf::Writer &getUtfWriter(lua_State *L) {
  writerSlot.get(L);

  if (lua_isnil(L, -1)) {
    throw std::runtime_error("write_* called in invalid scope");
  }

  utf::Writer &w = *static_cast<utf::Writer*>(lua_touserdata(L, -1));
  lua_pop(L, 1);

  return w;
}

static int writeLine(lua_State *L) {
  std::u32string &str = getUserData<std::u32string>(L, 1);
  getUtfWriter(L);

  utf::Writer &out = getUtfWriter(L);
  out.writeLine(str);

  return 0;
}

static int writeString(lua_State *L) {
  std::u32string &str = getUserData<std::u32string>(L, 1);
  utf::Writer &out = getUtfWriter(L);
  out.writeString(str);

  return 0;
}

static int writeInt(lua_State *L) {
  int i = lua_tointeger(L, 1);
  utf::Writer &out = getUtfWriter(L);
  out.writeInt(i);

  return 0;
}

static int writeChar(lua_State *L) {
  char32_t ch = getChar32(L, 1);
  utf::Writer &out = getUtfWriter(L);
  out.writeChar(ch);

  return 0;
}

static int writeNewline(lua_State *L) {
  utf::Writer &out = getUtfWriter(L);
  out.writeNewline();

  return 0;
}

static const luaL_Reg writerF[] = {
  { "write_line",    LF<writeLine>    },
  { "write_string",  LF<writeString>  },
  { "write_char",    LF<writeChar>    },
  { "write_int",     LF<writeInt>     },
  { "write_newline", LF<writeNewline> },
  LUA_REG_END
};

void writerRegister(lua_State *L) {
  lua_getglobal(L, "Tep");
  luaL_setfuncs(L, writerF, 0);
  lua_pop(L, 1);
}

}
}
