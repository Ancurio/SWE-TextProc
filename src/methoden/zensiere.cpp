#include <memory>
#include <cassert>
#include "method-registry-intern.h"
#include "utf.h"
#include "regex.h"
#include "io.h"

using namespace tep;
using namespace regex;

enum Modus {
  Black,
  White,
};

static const method::Desc methodDesc = {
  U"Zensiere jede Eingabezeile mittels einer Black- oder Whitelist.",
  {
    {
      param::Enum,
      U"modus",
      U"Die Art der Liste.\n"
      U"black: Zensiere alle Muster in der Liste\n"
      U"white: Zensiere alles außer den Mustern in der Liste",
      {
        U"black",
        U"white",
      },
    },
    {
      param::String,
      U"liste",
      U"Dateipfad der Liste",
    },
    {
      param::Character,
      U"zensurzeichen",
      U"Das Zeichen, welches zensierte Wörter ersetzt",
    },
  }
};

class Zensiere : public method::Base {
public:
  Zensiere(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  Modus modus;
  std::vector<Pattern::Handle> muster;
  char32_t zensurzeichen;
};

Zensiere::Zensiere(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  modus = p.param[0].getEnum<Modus>();
  std::u32string liste = p.param[1].getString();
  zensurzeichen = p.param[2].getCharacter();

  // XXX fange Fehler ab
  std::vector<std::u32string> entries = io::readListFile(liste);

  for (auto entry : entries) {
    muster.push_back(Pattern::construct(entry));
  }
}

void Zensiere::processLine(const std::u32string &zeile) {
  /* Markiere für jedes Zeichen, ob es von einem der Muster erkannt wird */
  std::vector<bool> marked(zeile.size(), false);

  for (const Pattern::Handle &p : muster) {
    const std::vector<Pattern::Range> locMatches = p->findMatches(zeile);

    for (const Pattern::Range &m : locMatches) {
      for (size_t i = m.begin; i != m.end; ++i) {
        marked[i] = true;
      }
    }
  }

  std::u32string ausg = zeile;

  for (size_t i = 0; i < ausg.size(); ++i) {
    char32_t &ch = ausg[i];

    /* Leerzeichen und Tabs sind nie von Zensur betroffen */
    if (ch == U' ' || ch == U'\t') {
      continue;
    }

    /* Blacklist: Erkannte Zeichen werden zensiert */
    if (modus == Black && marked[i]) {
      ch = zensurzeichen;
    }

    /* Whitelist: Nicht erkannte Zeichen werden zensiert */
    if (modus == White && !marked[i]) {
      ch = zensurzeichen;
    }
  }

  out.writeLine(ausg);
}

REGISTER_METHOD(Zensiere, U"zensiere", methodDesc)
