#include <memory>
#include <cassert>
#include "method-registry-intern.h"
#include "utf.h"
#include "util.h"

using namespace tep;

static const method::Desc methodDesc = {
  U"Teile jede Eingabezeile an einem Trennzeichen auf, und setze\n"
  U"die entstehenden Teile anhand einer Indexliste wider zusammen.",
  {
    {
      param::Character,
      U"an",
      U"Zeichen, an dem die Zeile geteilt wird",
    },
    {
      param::IntList,
      U"indizes",
      U"Die Reihenfolge der auszugebenen Teilstrings",
    },
  }
};

class Teile : public method::Base {
public:
  Teile(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  char32_t trennzeichen;
  std::vector<int> indizes;
};

Teile::Teile(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  trennzeichen = p.param[0].getCharacter();
  indizes = p.param[1].getIntList();
}

/* Wandle positive Indizes beginnend bei 1, in Indizes beginnend
 * bei 0 um. Wandle negative Indizes in äquivalente 0-basierte
 * positive Indizes um. Verwerfe "0" und out-of-range Indizes. */
static std::vector<int>
normalizeIndizes(const std::vector<int> &indizes, size_t size) {
  std::vector<int> result;

  for (int idx : indizes) {
    if (idx >= 0) {
      /* Mache 1-basierte positive Indizes 0-basierend.
       * Eine 0 wird hier zur -1, und dadurch weiter unten verworfen. */
      --idx;
    } else {
      /* Mache negative Indizes positiv 0-basierend */
      idx += size;
    }

    /* Sammle alle Indizes, die auf ein gültiges Teil zeigen. */
    if (idx >= 0 && idx < size) {
      result.push_back(idx);
    }
  }

  return result;
}

void Teile::processLine(const std::u32string &eingabe) {
  const std::vector<std::u32string> teile = util::split(eingabe, trennzeichen);
  const std::vector<int> normIndizes = normalizeIndizes(indizes, teile.size());
  std::u32string ausg;

  /* Füge Ausgabezeile zusammen */
  for (size_t i = 0; i < normIndizes.size(); ++i) {

    /* Füge vor dem ersten Teil kein Trennzeichen ein */
    if (i > 0) {
      ausg.append(1, trennzeichen);
    }

    ausg.append(teile[normIndizes[i]]);
  }

  out.writeLine(ausg);
}

REGISTER_METHOD(Teile, U"teile", methodDesc)
