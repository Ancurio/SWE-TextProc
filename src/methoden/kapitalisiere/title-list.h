#ifndef TITLE_LIST_H
#define TITLE_LIST_H

#include <unordered_set>
#include <string>

extern const std::unordered_set<std::u32string> titleLowerList;

#endif // TITLE_LIST_H
