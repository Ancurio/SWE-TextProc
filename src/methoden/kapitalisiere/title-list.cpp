#include "title-list.h"

const std::unordered_set<std::u32string> titleLowerList = {
  U"der", U"die", U"das", U"ein", U"eine",
  U"dieser", U"dieses", U"dieser", U"jene", U"jener",
  U"jenes", U"derjenige", U"diejenige", U"dasjenige", U"derselbe",
  U"dieselbe", U"dasselbe", U"derlei", U"dergleichen", U"mein",
  U"dein", U"sein", U"ihr", U"unsere", U"eurer",
  U"deren", U"dessen", U"welcher", U"welches", U"welche",
  U"wessen", U"alle", U"aller", U"alles", U"einige",
  U"einiger", U"einiges", U"etlicher", U"etlicher", U"etliches",
  U"irgendeine", U"irgendein", U"irgendwelche", U"irgendwelches", U"irgendwelcher",
  U"jede", U"jeder", U"jedes", U"jedweder", U"jedwede",
  U"jedwedes", U"kein", U"keine", U"mancher", U"manches",
  U"manche", U"mehrere", U"aber", U"allein", U"außer",
  U"ausser", U"beziehungsweise", U"denn", U"doch", U"hingegen",
  U"jedoch", U"oder", U"sondern", U"sowie", U"und",
  U"wie", U"als", U"damit", U"je", U"sodass",
  U"während", U"dass", U"nachdem", U"sofern", U"weil",
  U"ehe", U"ob", U"solange", U"wenn", U"falls",
  U"obgleich", U"sooft", U"auch", U"anstatt", U"geschweige",
  U"obschon", U"soviel", U"wenngleich", U"anstatt", U"gleichwie",
  U"obwohl", U"soweit", U"wie", U"indem", U"obzwar",
  U"sowenig", U"indes", U"ohne", U"sowie", U"wiewohl",
  U"bevor", U"indessen", U"seit", U"statt", U"wo",
  U"bis", U"insofern", U"seitdem", U"wohingegen", U"da",
  U"insoweit", U"sobald", U"trotzdem", U"zumal", U"zu",
  U"entweder", U"nicht", U"nur", U"sonder", U"sowohl",
  U"weder", U"noch", U"je", U"desto", U"umso",
  U"gleichwie", U"ab", U"abseits", U"an", U"auf",
  U"aus", U"außerhalb", U"ausserhalb", U"bei", U"bis",
  U"diesseits", U"durch", U"entlang", U"fern", U"gegen",
  U"gegenüber", U"hinter", U"in", U"inmitten", U"innerhalb",
  U"jenseits", U"längs", U"nach", U"nahe", U"neben",
  U"oberhalb", U"seitlich", U"über", U"um", U"unfern",
  U"unter", U"unterhalb", U"unweit", U"von", U"vor",
  U"zu", U"zwischen", U"binnen", U"für", U"mit",
  U"abzüglich", U"ausschließlich", U"ausschliesslich", U"zuzüglich", U"wider",
  U"samt", U"nebst", U"mitsamt", U"inklusive", U"exklusive",
  U"entgegen", U"einschließlich", U"einschliesslich", U"angesichts", U"anlässlich",
  U"betreffs", U"bezüglich", U"dank", U"gemäß", U"gemäss",
  U"halber", U"infolge", U"kraft", U"laut", U"mangels",
  U"mittels", U"seitens", U"trotz", U"willen", U"ungeachtet",
  U"vermittels", U"vermittelst", U"wegen", U"zufolge", U"zwecks",

};
