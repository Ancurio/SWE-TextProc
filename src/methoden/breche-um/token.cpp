#include <iostream>
#include <cassert>
#include "token.h"
#include "util.h"
#include "utf.h"
#include "io.h"

namespace tep {
namespace brecheum {

static TokenType chClass(char32_t ch) {
  if (ch == U' ' || ch == U'\t') {
    return WhiteSpace;
  }

  if (util::isGermanLetter(ch)) {
    return Word;
  }

  return TextBlob;
}

void TokenStream::appendNewline() {
  static Token t = { Newline };
  tokens.push_back(t);
}

void TokenStream::appendWhitespace() {
  static Token t = { WhiteSpace };
  tokens.push_back(t);
}

void TokenStream::appendWord(const std::u32string &word) {
  static Token t = { Word };
  t.data = word;
  tokens.push_back(t);
}

void TokenStream::appendTextBlob(const std::u32string &blob) {
  static Token t = { TextBlob };
  t.data = blob;
  tokens.push_back(t);
}

void TokenStream::appendToken(const Token &t) {
  tokens.push_back(t);
}

void TokenStream::parseString(const std::u32string &input) {
  enum {
    Undefined,
    InsideSpace,
    BeginPunct,
    InsideWord,
    EndPunct,
    InsideBlob,
  } state = Undefined;

  std::u32string data;
  tokens.clear();

  const int wordOrBlob = Word | TextBlob;

  for (const char32_t ch : input) {
  switch (state) {
  case Undefined:
    switch (chClass(ch)) {
    case Newline:
      assert(!"unreachable");
      break;

    case WhiteSpace:
      state = InsideSpace;
      break;

    case Word:
      state = InsideWord;
      data.push_back(ch);
      break;

    case TextBlob:
      state = BeginPunct;
      data.push_back(ch);
      break;
    }

    break;

  case InsideSpace:
    if (chClass(ch) != WhiteSpace) {
      appendWhitespace();
    }

    switch (chClass(ch)) {
    case Newline:
      assert(!"unreachable");
      break;

    case WhiteSpace:
      break;

    case Word:
      data.push_back(ch);
      state = InsideWord;
      break;

    case TextBlob:
      data.push_back(ch);
      state = BeginPunct;
      break;
    }

    break;

  case InsideWord:
    if (!(chClass(ch) & wordOrBlob)) {
      appendWord(data);
      data.clear();
    }

    switch (chClass(ch)) {
    case Newline:
      assert(!"unreachable");
      break;

    case WhiteSpace:
      state = InsideSpace;
      break;

    case Word:
      data.push_back(ch);
      break;

    case TextBlob:
      data.push_back(ch);
      state = EndPunct;
      break;
    }

    break;

  case InsideBlob:
    if (!(chClass(ch) & wordOrBlob)) {
      appendTextBlob(data);
      data.clear();
    }

    switch (chClass(ch)) {
    case Newline:
      assert(!"unreachable");
      break;

    case WhiteSpace:
      state = InsideSpace;
      break;

    case Word:
    case TextBlob:
      data.push_back(ch);
      break;
    }

    break;

  case BeginPunct:
    switch (chClass(ch)) {
    case Newline:
      assert(!"unreachable");
      break;

    case WhiteSpace:
      appendTextBlob(data);
      data.clear();
      state = InsideSpace;
      break;

    case Word:
      data.push_back(ch);
      state = InsideWord;
      break;

    case TextBlob:
      data.push_back(ch);
      state = InsideBlob;
      break;
    }

    break;

  case EndPunct:
    switch (chClass(ch)) {
    case Newline:
      assert(!"unreachable");
      break;

    case WhiteSpace:
      appendWord(data);
      data.clear();
      state = InsideSpace;
      break;

    case Word:
      data.push_back(ch);
      state = InsideBlob;
      break;

    case TextBlob:
      char32_t tmp = data.back();
      data.pop_back();
      appendWord(data);
      data.clear();
      data.push_back(tmp);
      data.push_back(ch);
      state = InsideBlob;
      break;
    }

    break;
  } /* switch (state) */
  } /* for (const char32_t ch : input) */

  switch (state) {
  case EndPunct:
  case InsideWord:
    appendWord(data);
    break;

  case BeginPunct:
  case InsideBlob:
    appendTextBlob(data);
    break;

  default:
    break;
  }
}

void TokenStream::trimTrailingSpace() {
  if (tokens.empty() || tokens.back().type != WhiteSpace) {
    return;
  }

  tokens.pop_back();
}

void TokenStream::finishCurrentLine() {
  static Token newlToken = { Newline };

  if (tokens.empty() || tokens.back().type == Newline) {
    return;
  }

  trimTrailingSpace();
  appendNewline();
}

void TokenStream::write(utf::Writer &out) {
  for (const Token t : tokens) {
    switch (t.type) {
    case Newline:
      out.writeNewline();
      break;

    case WhiteSpace:
      out.writeChar(U' ');
      break;

    case Word:
    case TextBlob:
      out.writeString(t.data);
      break;
    }
  }
}

void TokenStream::dump() {
  for (const Token &t : tokens) {
  const std::string u8data = utf::convert(t.data);

  switch (t.type) {
  case Newline:
    io::cerr << "[n]\n";
    break;

  case WhiteSpace:
    io::cerr << "[s]";
    break;

  case Word:
    io::cerr << "[w\"" << u8data << "\"]";
    break;

  case TextBlob:
    io::cerr << "[b\"" << u8data << "\"]";
    break;
  }
  }

  io::cerr << io::endl;
}

}
}
