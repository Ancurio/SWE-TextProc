#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <vector>

namespace tep {

namespace utf {
class Writer;
}

namespace brecheum {

enum TokenType {
  Newline    = (1 << 0),
  /* Leerzeichen oder Tabulator */
  WhiteSpace = (1 << 1),
  /* Deutsches Wort */
  Word       = (1 << 2),
  /* Wort mit Sonderzeichen (=> untrennbar) */
  TextBlob   = (1 << 3),
};

/* Ein Token stellt einen Baustein einer Zeile dar */
struct Token {
  TokenType type;
  std::u32string data;

  /* Tatsächliche Länge auf der Ausgabe */
  size_t length() const {
    switch (type) {
    case Newline:
      return 0;
    case WhiteSpace:
      return 1;

    case Word:
    case TextBlob:
      return data.size();
    }
  }
};

/* Hilfsklasse, um eine Liste von Tokens zu verwalten */
struct TokenStream {
  std::vector<Token> tokens;

  /* Füge den jeweiligen Tokentyp hinzu */
  void appendNewline();
  void appendWhitespace();
  void appendWord(const std::u32string &appendWord);
  void appendTextBlob(const std::u32string &blob);
  void appendToken(const Token &t);

  /* Spalte 'input' in Tokens auf und speichere
   * sie in diesem Objekt */
  void parseString(const std::u32string &input);

  /* Entferne Whitespace am Ende der letzten Zeile */
  void trimTrailingSpace();

  /* Äquivalent zu:
   *   trimTrailingSpace();
   *   appendNewline();
   *
   * Fügt keine neue Zeile hinzu, falls die aktuelle
   * Zeile noch leer ist */
  void finishCurrentLine();

  /* Schreibe den Inhalt aller Tokens auf 'out' */
  void write(utf::Writer &out);

  /* Gebe eine detailierte Inhaltsansicht auf stderr aus */
  void dump();
};

}
}

#endif // TOKEN_H
