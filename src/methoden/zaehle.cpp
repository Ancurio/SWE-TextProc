#include <memory>
#include <cassert>
#include <cstdint>
#include "method-registry-intern.h"
#include "utf.h"
#include "regex.h"

using namespace tep;

static const method::Desc methodDesc = {
  U"Zähle alle Vorkomnisse eines Musters im Eingabetext.\n"
  U"Die Anzahl wird ohne folgenden Zeilenumbruch ausgegeben.",
  {
    {
      param::Pattern,
      U"muster",
      U"Das zu zaehlende Muster",
    },
  }
};

class Zaehle : public method::Base {
public:
  Zaehle(ConstructParam &p);
  void processLine(const std::u32string &zeile);
  void finalize();

  regex::Pattern::Handle muster;
  uint64_t count;
};

Zaehle::Zaehle(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  muster = p.param[0].getPattern();
  count = 0;
}

void Zaehle::processLine(const std::u32string &zeile) {
  const std::vector<regex::Pattern::Range> matches =
      muster->findMatches(zeile);

  count += matches.size();
}

void Zaehle::finalize() {
  out.writeUInt(count);
}

REGISTER_METHOD(Zaehle, U"zaehle", methodDesc)
