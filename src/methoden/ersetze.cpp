#include <memory>
#include <cassert>
#include <boost/range/adaptor/reversed.hpp>
#include "method-registry-intern.h"
#include "utf.h"
#include "regex.h"

using namespace tep;
using namespace regex;

static const method::Desc methodDesc = {
  U"Ersetze alle Vorkomnisse eines Musters mit einer Zeichenkette.",
  {
    {
      param::Pattern,
      U"muster",
      U"Das zu ersetzende Muster",
    },
    {
      param::String,
      U"durch",
      U"Die ersetzende Zeichenkette",
    },
  }
};

class Ersetze : public method::Base {
public:
  Ersetze(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  Pattern::Handle muster;
  std::u32string mit;
};

Ersetze::Ersetze(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  muster = p.param[0].getPattern();
  mit = p.param[1].getString();
}

void Ersetze::processLine(const std::u32string &zeile) {
  const std::vector<Pattern::Range> matches =
      muster->findMatches(zeile);

  std::u32string ausg = zeile;

  /* Ersetze die erkannten Bereiche rückwerts, damit sich die
   * verbleibenden Indizes weiter vorne nicht verschieben */
  for (const Pattern::Range &m : boost::adaptors::reverse(matches)) {
    ausg.replace(m.begin, m.end - m.begin, mit);
  }

  out.writeLine(ausg);
}

REGISTER_METHOD(Ersetze, U"ersetze", methodDesc)
