#include <memory>
#include <cassert>
#include "method-registry-intern.h"
#include "utf.h"

using namespace tep;

/* Beispiel für die Einbindung der Verarbeitungsmethoden
 * ins Projekt mittels der Modulregistration */

static const method::Desc methodDesc = {
  U"Ein Beispiel für eine in C++ implementierte Methode",
  {
    {
      param::String,
      U"name",
      U"Der Name der Benutzerin/des Benutzers",
    },
  }
};

class GebeAus : public method::Base {
public:
  GebeAus(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  std::u32string halloName;
};

GebeAus::GebeAus(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  halloName = p.param[0].getString();
}

void GebeAus::processLine(const std::u32string &zeile) {
  /* Verarbeitung der Textzeile... */
  std::u32string ausg = U"Hallo " + halloName + U": " + zeile;
  out.writeLine(ausg);
}

#ifndef NDEBUG
REGISTER_METHOD(GebeAus, U"gebe-aus", methodDesc)
#endif
