#include <cassert>
#include <utility>
#include <unordered_set>
#include "method-registry-intern.h"
#include "utf.h"
#include "util.h"
#include "title-list.h"

using namespace tep;

enum Modus {
  Lower,
  Upper,
  Invert,
  Title
};

static const method::Desc methodDesc = {
  U"Forme die Kapitalisierungen von deutschen Wörtern um.",
  {
    {
      param::Enum,
      U"modus",
      U"Die Art der Umkapitalisierung",
      {
        U"lower",
        U"upper",
        U"invert",
        U"title",
      },
    },
  }
};

class Kapitalisiere : public method::Base {
public:
  Kapitalisiere(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  void titleCase(std::u32string &buffer);

  Modus modus;
};

Kapitalisiere::Kapitalisiere(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  modus = p.param[0].getEnum<Modus>();
}

void Kapitalisiere::processLine(const std::u32string &zeile) {
  int len = zeile.length();
  char32_t currentChar;
  std::u32string temp = zeile;

  if (modus == Lower) {   //Forme alle Buchstaben zu Kleinbuchstaben um.
    for (char32_t &ch : temp) {
      ch = util::toGermanLower(ch);
    }
  } else if (modus == Upper) {   //Forme alle Buchstaben zu Großbuchstaben um.
    for (char32_t &ch : temp) {
      ch = util::toGermanUpper(ch);
    }
  } else if (modus == Invert) {    //Forme alle Groß- zu Kleinbuchstaben um, und vise verse.
    for (char32_t &ch : temp) {
      if (util::isGermanLower(ch)) {
        ch = util::toGermanUpper(ch);
      } else { /* util::isGermanUpper(ch) */
        ch = util::toGermanLower(ch);
      }
    }
  } else if (modus == Title) {
    titleCase(temp);
  }

  out.writeLine(temp);
}

typedef std::u32string::iterator Iter;
typedef std::pair<Iter, Iter> Word; // <begin, end>

static std::vector<Word> scanGerWords(std::u32string &str) {
  Iter i = str.begin();
  std::vector<Word> result;

  while (i != str.end()) {
    /* Gehe bis zum Beginn des nächsten Wortes */
    while (i != str.end() && !util::isGermanLetter(*i)) {
      ++i;
    }

    if (i == str.end()) {
      /* Beim Suchen wurde das Ende des Strings erreicht:
       * keine weiteren Wörter vorhanden */
       return result;
    }

    Word w;
    w.first = i;

    /* Gehe bis zum Ende des aktuellen Wortes */
    while (i != str.end() && util::isGermanLetter(*i)) {
      ++i;
    }

    /* 'i' zeigt nun auf das erste Zeichen nach dem Wort,
     * was nicht mehr Deutsch ist; damit haben wir das Wortende
     * gefunden */
    w.second = i;
    result.push_back(w);
  }

  return result;
}

static void wordLower(Word &w) {
  for (Iter i = w.first; i != w.second; ++i) {
    *i = util::toGermanLower(*i);
  }
}

static void wordTitle(Word &w) {
  *w.first = util::toGermanUpper(*w.first);

  for (Iter i = w.first + 1; i != w.second; ++i) {
    *i = util::toGermanLower(*i);
  }
}

typedef std::unordered_set<std::u32string> U32StringSet;
static const U32StringSet &lowerListRef = titleLowerList;

void Kapitalisiere::titleCase(std::u32string &buffer) {
  std::vector<Word> gerWords = scanGerWords(buffer);

  for (Word &w : gerWords) {
    std::u32string wStr(w.first, w.second);
    wStr = util::toGermanLower(wStr);

    if (lowerListRef.count(wStr) > 0) {
      wordLower(w);
    } else {
      wordTitle(w);
    }
  }

  /* Das Wort am Satzanfang wird immer großgeschrieben */
  if (gerWords.size() > 0) {
    Iter i = gerWords.front().first;

    if (i == buffer.begin()) {
      *i = util::toGermanUpper(*i);
    }
  }
}

REGISTER_METHOD(Kapitalisiere, U"kapitalisiere", methodDesc)
