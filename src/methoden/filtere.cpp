#include <memory>
#include <cassert>
#include "method-registry-intern.h"
#include "utf.h"
#include "regex.h"

using namespace tep;

enum Modus {
  Mit,
  Ohne,
};

static const method::Desc methodDesc = {
  U"Filtere alle Eingabezeilen nach einem Muster.",
  {
    {
      param::Enum,
      U"modus",
      U"Welche Zeilen gefiltert werden.\n"
      U"mit:  Gebe alle Zeilen aus, in denen das Muster vorkommt.\n"
      U"ohne: Gebe alle Zeilen aus, in denen das Muster NICHT vorkommt.",
      {
        U"mit",
        U"ohne",
      },
    },
    {
      param::Pattern,
      U"muster",
      U"Das zu Filterende Muster",
    },
  }
};

class Filtere : public method::Base {
public:
  Filtere(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  Modus modus;
  regex::Pattern::Handle muster;
};

Filtere::Filtere(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  modus = p.param[0].getEnum<Modus>();
  muster = p.param[1].getPattern();
}

void Filtere::processLine(const std::u32string &zeile) {
  const std::vector<regex::Pattern::Range> matches =
      muster->findMatches(zeile);

  /* Zeilen mit Muster: Gebe aus, falls es Matches gab */
  if (modus == Mit && !matches.empty()) {
    out.writeLine(zeile);
  }

  /* Zeilen ohne Muster: Gebe aus, falls es keine Matches gab */
  if (modus == Ohne && matches.empty()) {
    out.writeLine(zeile);
  }
}

REGISTER_METHOD(Filtere, U"filtere", methodDesc)
