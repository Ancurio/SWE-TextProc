#include <memory>
#include <cassert>
#include <stdexcept>
#include "method-registry-intern.h"
#include "utf.h"
#include "hyphenation.h"
#include "token.h"
#include "util.h"

using namespace tep;
using namespace brecheum;

enum Worttrennung {
  Ja,
  Nein,
};

static const method::Desc methodDesc = {
  U"Breche jede Eingabezeile so um, dass möglichst alle\n"
  U"entstehenden Ausgabezeilen eine maximale Länge nicht überschreiten.",
  {
    {
      param::Integer,
      U"zeilenlaenge",
      U"Die maximale Zeilenlaenge",
    },
    {
      param::Enum,
      U"worttrennung",
      U"Sollen deutsche Wörter getrennt werden?\n"
      U"Diese Option kann dazu führen, dass lange, untrennbare\n"
      U"Zeichenketten die maximale Zeilenlänge überschreiten.",
      {
        U"ja",
        U"nein",
      },
    },
  }
};

class BrecheUm : public method::Base {
public:
  BrecheUm(ConstructParam &p);
  void processLine(const std::u32string &zeile);

  void processInTokens(utf::Writer &out);

  /* Trenne Wort, welches vorne und hinten ein Satzzeichen
   * haben kann */
  std::vector<std::u32string> split(const std::u32string &word);

  /* Schreibe die aktuelle Zeile in 'outStream' auf die
   * Standardausgabe */
  void emitLine(utf::Writer &out);

  /* Maximale Zeilenlänge */
  size_t zLaenge;

  /* Sollen Wörter getrennt werden? */
  bool wTrenn;

  /* Eingabe Tokenstream */
  TokenStream inStream;

  /* Transformierter Tokenstream */
  TokenStream outStream;

  /* Aktuelle Zeilenlänge */
  size_t curLength;
};

BrecheUm::BrecheUm(ConstructParam &p)
  : Base(p) {
  assert(p.param.size() == methodDesc.param.size());
  int paramLaenge = p.param[0].getInteger();

  if (paramLaenge < 8) {
    throw std::runtime_error("Maximale Zeilenlaenge muss mindestens 8 sein");
  }

  zLaenge = static_cast<size_t>(paramLaenge);
  wTrenn = (p.param[1].getEnum<Worttrennung>() == Ja);
}

void BrecheUm::processInTokens(utf::Writer &out) {
  outStream.tokens.clear();
  curLength = 0;
  static const std::u32string hyphen = U"-";

  /* Flag, die angiebt, ob die aktuell beschriebene
   * Zeile noch leer ist */
  bool emptyLine = true;

  for (auto tIter = inStream.tokens.cbegin();
       tIter != inStream.tokens.cend(); ++tIter) {
    const Token &t = *tIter;

    /* Im geparsten Input dürfte nie ein Newline
     * auftreten */
    assert(t.type != Newline);

    /* Trimme Whitespace am Anfang der Zeile */
    if (emptyLine && t.type == WhiteSpace) {
      emptyLine = false;
      continue;
    }

    if (curLength + t.length() <= zLaenge) {
      /* Hinzufügen des aktuellen Zeichens überschreitet
       * nicht die maximale Zeilenlänge */
      outStream.appendToken(t);
      curLength += t.length();
      emptyLine = false;

      /* Randfall: Die aktuelle Zeile wurde komplett
       * ausgefüllt */
      if (curLength == zLaenge) {
        emitLine(out);
        emptyLine = true;
      }

      continue;
    }

    /* Das aktuelle Token kann kein Whitespace mehr sein, da es
     * sonst weiter oben die Zeile komplett ausgefüllt hätte */
    assert(t.type != WhiteSpace);

    /* TextBlob: Übernehme in nächste Zeile
     * Word: Versuche zu trennen, um Zeile auszufüllen */
    switch (t.type) {
    case Newline:
    case WhiteSpace:
      assert(!"unreachable");
      break;

    case TextBlob:
      emitLine(out);
      outStream.appendToken(t);
      curLength = t.length();
      emptyLine = false;

      break;

    case Word:
      const std::vector<std::u32string> hyph = split(t.data);
      assert(!hyph.empty());

      /* Randfall: Das Wort konnte nicht weiter aufgeteilt werden.
       * Behandle es wie einen Textblob. */
      if (hyph.size() == 1) {
        emitLine(out);
        outStream.appendToken(t);
        curLength = t.length();
        emptyLine = false;

        break;
      }

      /* Iterator über alle Wortteile */
      decltype(hyph.cbegin()) i;

      /* Versuche, so viel wie möglich des Wortes auf die
       * aktuelle Zeile zu schreiben */
      for (i = hyph.cbegin(); i != hyph.cend(); ++i) {
        /* Rechne auch den benötigten Bindestrich mit ein (+1) */
        if (curLength + i->size() + 1 > zLaenge) {
          break;
        }

        outStream.appendWord(*i);
        curLength += i->size();
      }

      /* Randfall: Nichtmal ein Wortteil hat auf die aktuelle
       * Zeile gepasst. Beende die aktuelle Zeile, und versuche
       * es auf der nächsten Zeile */
      if (i == hyph.cbegin()) {
        emitLine(out);
        emptyLine = true;

        /* Lass das aktuelle Wort nochmal durchlaufen */
        --tIter;

        break;
      }

      /* Wir müssten immernoch Wortteile übrig haben, die auf
       * die nächste Zeile kommen */
      assert(i != hyph.cend());

      /* Schreibe den Bindestrich */
      outStream.appendTextBlob(hyphen);
      emitLine(out);
      emptyLine = true;

      /* Schreibe den Rest des Wortes auf die nächste Zeile */
      for (; i != hyph.cend(); ++i) {

        /* Randfall: Beim überlaufen auf die nächste Zeile ist
         * diese auch voll geworden. Mache auf der übernächsten
         * Zeile weiter */
        if (curLength + i->size() >= zLaenge && (i+1) != hyph.cend()) {
          outStream.appendTextBlob(hyphen);
          emitLine(out);
          emptyLine = false;
        }

        outStream.appendWord(*i);
        curLength += i->size();
        emptyLine = false;
      }
    } /* switch (t.type) */

    /* Randfall: Übertrag auf die nächste Zeile
     * hat diese auch komplett ausgefüllt */
    if (curLength >= zLaenge) {
      emitLine(out);
      emptyLine = true;
    }
  } /* for (const Token &t : tokens) */
}

std::vector<std::u32string> BrecheUm::split(const std::u32string &word) {
  assert(!word.empty());
  std::vector<std::u32string> result;

  if (wTrenn) {
    if (util::isGermanLetter(word.front()) &&
        util::isGermanLetter(word.back())) {
      result = splitWord(word);
    } else {
      char32_t b = U'\0', e = U'\0';
      std::u32string tmp = word;

      /* Prüfe, ob vor dem Wort ein Satzzeichen steht */
      if (!util::isGermanLetter(word.front())) {
        b = word.front();
        tmp.erase(tmp.begin());
      }

      /* Prüfe, ob nach dem Wort ein Satzzeichen steht */
      if (!util::isGermanLetter(word.back())) {
        e = word.back();
        tmp.pop_back();
      }

      result = splitWord(tmp);

      if (b != U'\0') {
        /* Falls es vor dem Wort ein Satzzeichen gab,
         * füge es am Anfang des ersten Wortteils ein */
        result.front().insert(0, 1, b);
      }

      if (e != U'\0') {
        /* Falls es nach dem Wort ein Satzzeichen gab,
         * füge es am Ende des letzten Wortteils ein */
        result.back().push_back(e);
      }
    }
  } else {
    /* Worttrennung nicht aktiv: Wort bleibt ganz */
    result.push_back(word);
  }

  return result;
}

void BrecheUm::emitLine(utf::Writer &out) {
  outStream.finishCurrentLine();
  curLength = 0;

  outStream.write(out);
  outStream.tokens.clear();
}

void BrecheUm::processLine(const std::u32string &zeile) {
  inStream.parseString(zeile);

  if (inStream.tokens.empty()) {
    out.writeNewline();
    return;
  }

  // Todo: Immernur eine fest Anzahl an Tokens
  // (z.B. 1024) vom input parsen und verarbeiten
  processInTokens(out);

  /* Beende die letzte angefangene Zeile */
  emitLine(out);
}

REGISTER_METHOD(BrecheUm, U"breche-um", methodDesc)
