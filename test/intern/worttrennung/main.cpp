#include <iostream>
#include <iomanip>
#include <string>
#include <cassert>

#include "main.h"
#include "trennungen.h"
#include "hyphenation.h"
#include "lexical-algo.h"
#include "utf.h"
#include "io.h"

using namespace tep;

int tep::main(const std::vector<std::u32string> &args) {

  /* Erfolgreiche Trennungen */
  int success = 0;

  /* Gesamtzahl an Tests */
  int total = 0;

  const char *p = test::trennungen;

  io::cerr << "Starte Test.. (kann etwas dauern)"
            << io::endl << io::endl;

  bool debugMode = false;

  if (args.size() >= 2 && args[1] == U"--debug") {
    debugMode = true;
  }

  while (*p) {
    /* Das Wort (ohne '|' Trennzeichen) */
    std::string word;

    /* Die Referenztrennung des Wortes */
    std::string refHyph;

    for (; *p && *p != ';'; ++p) {
      refHyph.push_back(*p);

      if (*p != '|') {
        word.push_back(*p);
      }
    }

    assert(!refHyph.empty());
    assert(!word.empty());

    std::vector<std::u32string> wordParts =
      hyph::lexicalSplit(utf::convert(word));

    assert(!wordParts.empty());

    /* Die vom Code erstellte Trennung, mit '|' Trennzeichen */
    std::u32string actualHyph;

    for (const std::u32string &part : wordParts) {
      actualHyph.append(part);
      actualHyph.append(1, U'|');
    }

    /* Entferne '|' am Ende */
    actualHyph.pop_back();

    bool isCorrect = (utf::convert(refHyph) == actualHyph);
    success += isCorrect;
    ++total;

    if (debugMode && !isCorrect) {
      io::cerr << "Correct: " << refHyph << io::endl;
      hyph::lexicalSplit(utf::convert(word), true);
      io::cerr << io::endl;
    }

    ++p;
  }

  io::cerr << "Gesamt:      " << total << io::endl;
  io::cerr << "Erfolgreich: " << success
            << std::fixed << std::setprecision(2)
            << " (" << ((float) success / total) * 100 << "%)"
            << io::endl;

  return 0;
}
