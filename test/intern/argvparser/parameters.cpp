#include <exception>
#include "parameters.h"
#include "argvparser.h"
#include "utf.h"

namespace tep {
namespace test {

void Parameters::run() {
  argv::parse(mArg, mDesc, mVal);
}

void Parameters::reset() {
  mArg.clear();
  mDesc.clear();
  mVal.clear();
}

void Parameters::resetArg() {
  mArg.clear();
}

param::Value &Parameters::val(size_t idx) {
  if (idx >= mVal.size())
    throw std::runtime_error("Accessed value doesn't exist");

  return mVal[idx];
}

param::Value &Parameters::first() {
  return val(0);
}

void Parameters::pushArg(const std::string str) {
  mArg.push_back(utf::convert(str));
}

void Parameters::pushStringDesc(const std::string &name) {
  param::Desc d;
  d.type = param::String;
  d.name = utf::convert(name);

  mDesc.push_back(d);
}

void Parameters::pushCharDesc(const std::string &name) {
  param::Desc d;
  d.type = param::Character;
  d.name = utf::convert(name);

  mDesc.push_back(d);
}

void Parameters::pushPatternDesc(const std::string &name) {
  param::Desc d;
  d.type = param::Pattern;
  d.name = utf::convert(name);

  mDesc.push_back(d);
}

void Parameters::pushEnumDesc(const std::initializer_list<std::string> &valid,
                              const std::string &name) {
  param::Desc d;
  d.type = param::Enum;
  d.name = utf::convert(name);

  for (auto v : valid) {
    d.validEnums.push_back(utf::convert(v));
  }

  mDesc.push_back(d);
}

void Parameters::pushIntDesc(const std::string &name) {
  param::Desc d;
  d.type = param::Integer;
  d.name = utf::convert(name);

  mDesc.push_back(d);
}

void Parameters::pushIntListDesc(const std::string &name) {
  param::Desc d;
  d.type = param::IntList;
  d.name = utf::convert(name);

  mDesc.push_back(d);
}

}
}
