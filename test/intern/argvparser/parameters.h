#ifndef TESTPARAMETERS_H
#define TESTPARAMETERS_H

#include "parameter.h"

namespace tep {
namespace test {

/* Hilfsstruktur, die das hinzufügen von Parametern
 * an argv::parse erleichtert */
struct Parameters {

  /* Argumente aus der Kommandozeile */
  std::vector<std::u32string> mArg;

  /* Parameterbeschreibungen */
  std::vector<param::Desc> mDesc;

  /* Geparste Parameterwerte, die nach 'run()' geschrieben wurden */
  std::vector<param::Value> mVal;

  /* Führe argv::parse mit allen Parametern aus */
  void run();

  /* Lösche alle zu übergebenden Argumente */
  void reset();

  /* Lösche alle Kommandozeilenargumente */
  void resetArg();

  /* Führe einen Boundcheck durch und gib mVal[idx] zurück */
  param::Value &val(size_t idx);

  /* Führe einen Boundcheck durch und gib mVal[0] zurück */
  param::Value &first();

  /* Füge 'str' zu den Kommandozeilenargumenten hinzu */
  void pushArg(const std::string str);

  /* Füge eine Parameterbeschreibung des jeweiligen Typs hinzu */
  void pushStringDesc(const std::string &name = std::string());
  void pushCharDesc(const std::string &name = std::string());
  void pushPatternDesc(const std::string &name = std::string());
  void pushEnumDesc(const std::initializer_list<std::string> &valid,
                    const std::string &name = std::string());
  void pushIntDesc(const std::string &name = std::string());
  void pushIntListDesc(const std::string &name = std::string());
};

}
}

#endif // TESTPARAMETERS_H
