#include <iostream>

#include "main.h"
#include "parameter.h"
#include "argvparser.h"
#include "regex.h"
#include "utf.h"

#include "parameters.h"

#define BOOST_TEST_MODULE ArgvParserTest
#define BOOST_TEST_NO_MAIN
#include <boost/test/included/unit_test.hpp>

using namespace tep;
using namespace test;

/* Ein notwendiger Hack, damit boost die Typen ausgeben kann.
 * Siehe: http://stackoverflow.com/a/17573165/981148 */
namespace boost { namespace test_tools { namespace tt_detail {
template<>
struct print_log_value<std::u32string> {
  void operator() (std::ostream& os, const std::u32string &str) {
      os << utf::convert(str);
  }
};
template<>
struct print_log_value<char32_t> {
  void operator() (std::ostream& os, const char32_t &ch) {
      os << utf::convert(std::u32string(1, ch));
  }
};
template<>
struct print_log_value<std::vector<int> > {
  void operator() (std::ostream& os, const std::vector<int> &il) {
    for (int i : il) {
      os << i << " ";
    }
  }
};
}}}

/* Teste, dass beim Parsen wirklich die geforderte
 * Menge an Parametern herauskommt. Teste auch,
 * dass ein Fehler geworfen wird, falls die Anzahl
 * geforderter Parameter von der tatsächlichen abweicht. */
BOOST_AUTO_TEST_CASE(Cardinality)
{
  Parameters p;

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.mVal.size(), 0);

  p.pushArg("abc");
  p.pushStringDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.mVal.size(), 1);

  p.pushArg("2");
  p.pushIntDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.mVal.size(), 2);

  p.reset();

  /* Zu wenig Aktualparameter */
  p.pushArg("abc def p");
  p.pushStringDesc();
  p.pushStringDesc();
  p.pushCharDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.reset();

  /* Zu viele Aktualparameter */
  p.pushArg("abc");
  p.pushArg("def");
  p.pushArg("p");
  p.pushStringDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);
}

/* Testet die Typumwandlungen von u32string
 * in die jeweiligen Paramtertypen. Es werden nur
 * positive Fälle geprüft, d.h. das Argument ist
 * eine gültige Form des entsprechenden Typs. */
BOOST_AUTO_TEST_CASE(TypeConversionsPositive)
{
  Parameters p;

  /* == Strings == */
  p.pushArg("ASCII string");
  p.pushStringDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getString(), p.mArg[0]);

  p.reset();
  p.pushArg("Unicode öäüß 河童の誇り 🙈");
  p.pushStringDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getString(), p.mArg[0]);

  /* == Character == */
  p.reset();
  p.pushArg("A");
  p.pushCharDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getCharacter(), U'A');

  p.reset();
  p.pushArg("ß");
  p.pushCharDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getCharacter(), U'ß');

  /* == Enum == */
  p.reset();
  p.pushEnumDesc({"enum value", "another value"});

  p.pushArg("enum value");
  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getEnum<int>(), 0);

  p.mArg.clear();
  p.pushArg("another value");
  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getEnum<int>(), 1);

  /* == Integer == */
  p.reset();
  p.pushArg("1");
  p.pushIntDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getInteger(), 1);

  p.reset();
  p.pushArg("987");
  p.pushIntDesc();

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getInteger(), 987);

  /* Integerlisten umfassen soviele Fälle, dass sie
   * ihren eigenen Testcase bekommen */
}

/* Testet die Typumwandlungen von u32string
 * in die jeweiligen Paramtertypen. Es werden nur
 * negative Fälle geprüft, d.h. das Argument ist
 * KEINE gültige Form des entsprechenden Typs. */
BOOST_AUTO_TEST_CASE(TypeConversionsNegative)
{
  Parameters p;

  /* Alle Argumentenstrings sind natürlich automatisch
   * gültige Strings, also gibts da nichts zu testen */

  /* == Character ==
   * Darf nicht mehr als ein Zeichen sein */
  p.reset();
  p.pushArg("AB");
  p.pushCharDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.reset();
  p.pushArg("ßö");
  p.pushCharDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.reset();
  p.pushArg("123");
  p.pushCharDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);

  /* == Regex pattern ==
   * Prüfe eine nichtgeschlossene Klammerung */
  p.reset();
  p.pushArg("[A-Z");
  p.pushPatternDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);

  /* == Enum ==
   * Prüfe einen Wert, der nicht zu den gültigen
   * Enumwerten gehört */
  p.reset();
  p.pushArg("bad value");
  p.pushEnumDesc({"enum value", "another value"});

  BOOST_CHECK_THROW(p.run(), argv::Error);

  /* == Integer ==
   * Buchstaben lassen sich nicht zu Zahlen machen */
  p.reset();
  p.pushArg("ABC");
  p.pushIntDesc();

  BOOST_CHECK_THROW(p.run(), argv::Error);
}

/* Testet die Typumwandlungen von u32string
 * in ein Muster. Es werden nur
 * positive Fälle geprüft, d.h. das Argument ist
 * eine gültige Form des Musters. */
BOOST_AUTO_TEST_CASE(PatternConversionsPositive)
{
  Parameters p;

  /* Der check hier ist etwas aufwendiger,
   * da wir keine direkte equals Methode haben */
  p.pushArg("[A-Z]");
  p.pushPatternDesc();

  BOOST_CHECK_NO_THROW(p.run());
  std::shared_ptr<regex::Pattern> pat = nullptr;
  BOOST_CHECK(pat = p.first().getPattern());
  BOOST_REQUIRE(pat != nullptr);

  /* Prüfe, ob das Pattern richtig erkennt.
   * Positive tests: */
  BOOST_CHECK(!pat->findMatches(U"A").empty());
  BOOST_CHECK(!pat->findMatches(U"J").empty());
  BOOST_CHECK(!pat->findMatches(U"Z").empty());
  /* Negative tests: */
  BOOST_CHECK(pat->findMatches(U"a").empty());
}


/* Testet die Typumwandlungen von u32string
 * in eine IntList. Es werden nur
 * positive Fälle geprüft, d.h. das Argument ist
 * eine gültige Form der Intlist. */
BOOST_AUTO_TEST_CASE(IntlistConversionPositive)
{
  typedef std::vector<int> IL;
  Parameters p;
  p.pushIntListDesc();

  p.pushArg("11");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({11}));

  p.resetArg();
  p.pushArg("33,22,11");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({33,22,11}));

  p.resetArg();
  p.pushArg("-11,-22,-33");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({-11, -22, -33}));

  p.resetArg();
  p.pushArg("0");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({0}));

  p.resetArg();
  p.pushArg("1,0,-2");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({1,0,-2}));

  p.resetArg();
  p.pushArg("3/3");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({3}));

  p.resetArg();
  p.pushArg("-4/-4");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({-4}));

  p.resetArg();
  p.pushArg("11/14");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({11,12,13,14}));

  p.resetArg();
  p.pushArg("14/10");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({14,13,12,11,10}));

  p.resetArg();
  p.pushArg("-12/-15");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({-12,-13,-14,-15}));

  p.resetArg();
  p.pushArg("-15/-10");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({-15,-14,-13,-12,-11,-10}));

  p.resetArg();
  p.pushArg("3,4/1,-3,-3/-2,7/3");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({3,4,3,2,1,-3,-3,-2,7,6,5,4,3}));

  /* Betretung über 0 ist möglich */
  p.resetArg();
  p.pushArg("-2/2");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({-2,-1,0,1,2}));

  p.resetArg();
  p.pushArg("2/-2");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({2,1,0,-1,-2}));

  p.resetArg();
  p.pushArg("-2/0");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({-2,-1,0}));

  p.resetArg();
  p.pushArg("0/-2");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({0,-1,-2}));

  p.resetArg();
  p.pushArg("2/0");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({2,1,0}));

  p.resetArg();
  p.pushArg("0/2");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getIntList(), IL({0,1,2}));
}

/* Testet die Typumwandlungen von u32string
 * in eine IntList. Es werden nur
 * negative Fälle geprüft, d.h. das Argument ist
 * KEINE gültige Form einer IntList. */
BOOST_AUTO_TEST_CASE(IntlistConversionNegative)
{
  Parameters p;
  p.pushIntListDesc();

  p.pushArg("ABC");
  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.resetArg();
  p.pushArg("[1,2]");
  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.resetArg();
  p.pushArg("--1");
  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.resetArg();
  p.pushArg("/2");
  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.resetArg();
  p.pushArg("1/");
  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.resetArg();
  p.pushArg("0,/3,0");
  BOOST_CHECK_THROW(p.run(), argv::Error);

  p.resetArg();
  p.pushArg("0,4/,0");
  BOOST_CHECK_THROW(p.run(), argv::Error);
}

/* Teste die benannte Parametrisierung */
BOOST_AUTO_TEST_CASE(NamedParamPositive)
{
  Parameters p;

  p.pushStringDesc("someString");
  p.pushArg("--someString");
  p.pushArg("value");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.first().getString(), U"value");

  p.pushIntDesc("someInt");
  p.pushArg("--someInt");
  p.pushArg("42");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.val(0).getString(), U"value");
  BOOST_CHECK_EQUAL(p.val(1).getInteger(), 42);

  /* Umgekehrte Reihenfolge */
  p.resetArg();
  p.pushArg("--someInt");
  p.pushArg("42");
  p.pushArg("--someString");
  p.pushArg("value");

  BOOST_CHECK_NO_THROW(p.run());
  BOOST_CHECK_EQUAL(p.val(0).getString(), U"value");
  BOOST_CHECK_EQUAL(p.val(1).getInteger(), 42);
}

int tep::main(const std::vector<std::u32string> &args) {
  extern ::boost::unit_test::test_suite* init_unit_test_suite( int argc, char* argv[] );
  ::boost::unit_test::init_unit_test_func init_func = &::init_unit_test_suite;
  const char *argv[] = { "test-argvparser" };

  return ::boost::unit_test::unit_test_main( init_func, 1, (char**)argv );
}
