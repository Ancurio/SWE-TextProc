add_tests (
  simpel.test
  breche-um.test
  lastenheft.test
  ungueltige_zlaenge.test
  whitespace.test
  satzzeichen.test
  eingabe_umbruch.test
  zeile_ausgefuellt.test
  worttrennung_simpel.test
  mehrfacher_uebertrag.test
)
