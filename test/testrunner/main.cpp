#include <fstream>
#include <sstream>
#include <algorithm>
#include <iostream>
#include "main.h"
#include "test.h"
#include "directory.h"
#include "defines.h"
#include "io.h"

using namespace tep;
using namespace test;

struct Cmdline {
  std::string inFile;
  std::string outFile;

  std::string meth;
  std::vector<Parameter> param;
};

static std::string
buildCommandLine(const Cmdline &param) {
  std::stringstream cmd;

  cmd << '<' << param.inFile << ' ';
  cmd << TEP_EXE << ' ' << param.meth << ' ';

  for (const Parameter p : param.param) {
    cmd << "--" << p.first << ' ' << '"' << p.second << '"' << ' ';
  }

  cmd << '>' << param.outFile << ' ';

  cmd << "2>" << DEV_NULL;

  return cmd.str();
}

static bool runTest(const Test &t, const std::string &meth) {
  Cmdline cmdline;
  cmdline.inFile = "__testrunner_in";
  cmdline.outFile = "__testrunner_out";
  cmdline.meth = meth;
  cmdline.param = t.param;

  std::string cmd = buildCommandLine(cmdline);

  std::ofstream inFile(cmdline.inFile, std::ios_base::out);
  inFile << t.in << NATIVE_LF;
  inFile.close();

  for (const File &f : t.files) {
    std::ofstream auxFile(f.name, std::ios_base::out);
    auxFile << f.contents << NATIVE_LF;
    auxFile.close();
  }

  std::system(cmd.c_str());

  std::string outLine;
  std::string out;
  std::ifstream outFile(cmdline.outFile, std::ios_base::in);

  while (std::getline(outFile, outLine)) {
    out.append(outLine);
    out.append(1, '\n');
  }

  if (!out.empty()) {
    out.pop_back();
  }

  outFile.close();

  if (out == t.out) {
    return true;
  } else {
    io::cerr << "  Test fehlgeschlagen:" << io::endl;

    if (!t.description.empty()) {
      io::cerr << "    Beschreibung: " << t.description << io::endl;
    }

    io::cerr << "    Eingabe: " << '[' << t.in << ']' << io::endl;
    io::cerr << "    Parameter:" << io::endl;

    for (const Parameter &p : t.param) {
      io::cerr << "      " << p.first << ": " << p.second << io::endl;
    }

    io::cerr << "    Erwartet: " << '[' << t.out << ']' << io::endl;
    io::cerr << "    Erhalten: " << '[' << out << ']' << io::endl;

    return false;
  }
}

static bool hasNoTestExtension(const std::string &path) {
  const std::string ext(".test");

  if (path.size() < ext.size()) {
    return true;
  }

  return path.compare(path.size() - ext.size(), ext.size(), ext) != 0;
}

static bool checkTepAccessible() {
  std::ifstream tepExe(TEP_EXE);

  if (!tepExe.good()) {
    io::cerr << "Ausfuehrbare Datei '" << TEP_EXE
              << "' kann nicht gefunden werden." << io::endl
              << "Sicher, dass du im richtigen Verzeichnis bist?" << io::endl;

    return false;
  }

  return true;
}

int tep::main(const std::vector<std::u32string> &args) {
  if (!checkTepAccessible()) {
    return 0;
  }

  std::string testRoot = "test/extern";
  std::string filterMethod;

  if (args.size() > 1) {
    filterMethod = utf::convert(args[1]);
  }

  if (args.size() > 2) {
    testRoot = utf::convert(args[2]);
  }

  auto list = directory::collectFileList(testRoot);
  list.erase(std::remove_if(list.begin(), list.end(), hasNoTestExtension),
             list.end());

  io::cerr << list.size() << " Testmodule wurden erfasst" << io::endl << io::endl;

  int succsGlobal = 0;
  int countGlobal = 0;

  for (auto path : list) {
    std::fstream testFile(path, std::ios_base::in);
    TestModule mod;

    if (!testFile.is_open()) {
      continue;
    }

    try {
      mod = TestModule::parse(testFile);
    } catch (const std::runtime_error &exc) {
      io::cerr << "Fehler beim Parsen der Datei [" << path << "]:" << io::endl
                << exc.what() << io::endl << io::endl;
      continue;
    }

    if (!filterMethod.empty() && mod.methodName != filterMethod) {
      continue;
    }

    io::cerr << "Lasse " << mod.tests.size() << " Tests für ["
              << mod.methodName << "] laufen: " << mod.description << io::endl;

    int succs = 0;

    for (const Test &t : mod.tests) {
      succs += runTest(t, mod.methodName) ? 1 : 0;
    }

    succsGlobal += succs;
    countGlobal += mod.tests.size();

    io::cerr << succs << " erfolgreich, "
              << mod.tests.size() - succs << " fehlgeschlagen." << io::endl;
    io::cerr << io::endl;
  }

  io::cerr << "###" << io::endl
            << "Insgesamt " << succsGlobal << " Tests erfolgreich, "
            << countGlobal - succsGlobal << " fehlgeschlagen." << io::endl;

  return 0;
}
