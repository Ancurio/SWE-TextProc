set (
  RUNNER_SOURCES
  ../../src/kern/io.cpp
  ../../src/kern/utf.cpp
  ../../src/kern/directory.cpp
  main.cpp
  test.cpp
)

set (
  RUNNER_HEADERS
  test.h
)

add_executable (testrunner ${RUNNER_SOURCES} ${RUNNER_HEADERS})
