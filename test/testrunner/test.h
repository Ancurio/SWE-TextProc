#ifndef TEST_H
#define TEST_H

#include <istream>
#include <vector>
#include <utility>

namespace tep {
namespace test {

typedef std::pair<std::string, std::string> Parameter;

struct File {
  std::string name;
  std::string contents;
};

struct Test {
  /* Erforderlich */
  std::vector<Parameter> param;
  std::string in;
  std::string out;

  /* Optional */
  std::string description;
  std::vector<File> files;
};

struct TestModule {
  std::string methodName;
  std::string description;

  std::vector<Test> tests;

  /* Wirft std::runtime_error bei invalidem JSON */
  static TestModule parse(std::basic_istream<char> &src);
};

}
}

#endif // TEST_H
