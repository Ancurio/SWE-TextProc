#include <fstream>
#include <stdexcept>

#define BOOST_MULTI_INDEX_DISABLE_SERIALIZATION
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "test.h"

namespace pt = boost::property_tree;

using namespace tep;
using namespace test;

/* Lese einen String aus, der entweder direkt in "" Form vorliegt,
 * oder als Array, in dem die Zeilen aufgelistet sind.
 * Beispiel:
 * "parent": {
 *   "child1": "Normaler String",
 *   "child2": [
 *     "Mehrere",
 *     "Zeilen String"
 *   ]
 * }
 *
 * getMultilineString(parent.get_child("child1")); // => "Normaler String"
 * getMultilineString(parent.get_child("child2")); // => "Mehrere\nZeilen String"
 *
 * Natürlich kann man Umbrüche auch inline mit \n in einem String angeben,
 * aber das ist von der Leserlichkeit her bei weitem nicht so gut.
 */
static std::string
getMultilineString(const pt::ptree &node) {
  pt::ptree::const_iterator iter = node.begin();
  std::string result = node.data();

  for (const pt::ptree::value_type &line : node) {
    result.append(line.second.data());
    result.append(1, '\n');
  }

  /* Entferne letzten Linefeed */
  if (!result.empty() && result.back() == '\n') {
    result.pop_back();
  }

  return result;
}

static Test parseTest(const pt::ptree &node) {
  Test t;

  t.description = node.get<std::string>("beschreibung", "");

  for (const pt::ptree::value_type &p : node.get_child("parameter")) {
    t.param.push_back(Parameter(p.first, p.second.data()));
  }

  boost::optional<const pt::ptree&> files = node.get_child_optional("dateien");

  if (files) {
    for (const pt::ptree::value_type &f : files.get()) {
      t.files.push_back(File{f.first, getMultilineString(f.second)});
    }
  }

  t.in = getMultilineString(node.get_child("ein"));
  t.out = getMultilineString(node.get_child("aus"));

  return t;
}

TestModule TestModule::parse(std::basic_istream<char> &src) {
  TestModule mod;
  pt::ptree tree;

  try {
    pt::read_json(src, tree);
  } catch (const pt::json_parser_error &exc) {
    throw std::runtime_error(exc.what());
  }

  try {
    mod.methodName = tree.get<std::string>("methode");
    mod.description = tree.get<std::string>("beschreibung");

    for (const pt::ptree::value_type &t : tree.get_child("tests")) {
      mod.tests.push_back(parseTest(t.second));
    }
  } catch (const pt::ptree_bad_path &exc) {
    throw std::runtime_error(exc.what());
  }

  return mod;
}
